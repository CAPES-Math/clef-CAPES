#!/usr/bin/env bash

# key version
VERSION=0.0.1

# Configuration to use
CONFIG_SRC=capes_config

# Size of the final image in GB
SIZE=3

# Where the build will occur
BUILD_DIR=/build

# Project name
PROJECT_NAME=clef
PROJECT_DIR=$BUILD_DIR/$PROJECT_NAME

# Architecture
ARCHITECTURE=amd64

# Copy context at the end
USE_CONTEXT=true
