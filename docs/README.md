# Télécharger le thème

```
cd themes
git clone digitalcraftsman/hugo-material-docs.git 
```

# Tester localement

```
hugo server
```

# Construire

```
hugo
```


