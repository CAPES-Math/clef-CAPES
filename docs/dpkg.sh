#!/usr/bin/env bash

# generates the dpkg list
dpkg=$(ssh -l root localhost -p 12345 "dpkg -l")
echo -e "\`\`\`\n$dpkg\n\`\`\`" > content/caracteristiques/dpkg-l.md
