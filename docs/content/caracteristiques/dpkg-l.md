```
Souhait=inconnU/Installé/suppRimé/Purgé/H=à garder
| État=Non/Installé/fichier-Config/dépaqUeté/échec-conFig/H=semi-installé/W=attend-traitement-déclenchements
|/ Err?=(aucune)/besoin Réinstallation (État,Err: majuscule=mauvais)
||/ Nom                                   Version                              Architecture Description
+++-=====================================-====================================-============-===============================================================================
ii  acl                                   2.2.52-2                             amd64        Access control list utilities
ii  adduser                               3.113+nmu3                           all          add and remove users and groups
ii  adobeair                              1:2.6.0.2                            amd64        Adobe AIR 2
ii  adwaita-icon-theme                    3.14.0-2                             all          default icon theme of GNOME
ii  alsa-base                             1.0.27+1                             all          dummy package to ease purging of obsolete conffiles
ii  alsa-utils                            1.0.28-1                             amd64        Utilities for configuring and using ALSA
ii  anacron                               2.3-23                               amd64        cron-like program that doesn't go by time
ii  ant                                   1.9.4-3                              all          Java based build tool like make
ii  ant-optional                          1.9.4-3                              all          Java based build tool like make - optional libraries
ii  apt                                   1.0.9.8.4                            amd64        commandline package manager
ii  apt-utils                             1.0.9.8.4                            amd64        package management related utility programs
ii  aspcud                                1:1.9.0-2                            amd64        CUDF solver based on Answer Set Programming
ii  aspell                                0.60.7~20110707-1.3                  amd64        GNU Aspell spell-checker
ii  aspell-en                             7.1-0-1.1                            all          English dictionary for GNU Aspell
ii  at-spi2-core                          2.14.0-1                             amd64        Assistive Technology Service Provider Interface (dbus core)
ii  avahi-daemon                          0.6.31-5                             amd64        Avahi mDNS/DNS-SD daemon
ii  base-files                            8+deb8u7                             amd64        Debian base system miscellaneous files
ii  base-passwd                           3.5.37                               amd64        Debian base system master password and group files
ii  bash                                  4.3-11+deb8u1                        amd64        GNU Bourne Again SHell
ii  bind9-host                            1:9.9.5.dfsg-9+deb8u9                amd64        Version of 'host' bundled with BIND 9.X
ii  binutils                              2.25-5                               amd64        GNU assembler, linker and binary utilities
ii  broadcom-sta-dkms                     6.30.223.248-3                       all          dkms source for the Broadcom STA Wireless driver
ii  bsdmainutils                          9.0.6                                amd64        collection of more utilities from FreeBSD
ii  bsdutils                              1:2.25.2-6                           amd64        basic utilities from 4.4BSD-Lite
ii  build-essential                       11.7                                 amd64        Informational list of build-essential packages
ii  busybox                               1:1.22.0-9+deb8u1                    amd64        Tiny utilities for small and embedded systems
ii  bzip2                                 1.0.6-7+b3                           amd64        high-quality block-sorting file compressor - utilities
ii  ca-certificates                       20141019+deb8u2                      all          Common CA certificates
ii  ca-certificates-java                  20140324                             all          Common CA certificates (JKS keystore)
ii  camlp4                                4.01.0-5                             amd64        Pre Processor Pretty Printer for OCaml
ii  clasp                                 3.1.0-1                              amd64        conflict-driven nogood learning answer set solver
ii  coinor-libcbc3                        2.8.12-1                             amd64        Coin-or branch-and-cut mixed integer programming solver (shared libraries)
ii  coinor-libcgl1                        0.58.9-1                             amd64        COIN-OR Cut Generation Library
ii  coinor-libclp1                        1.15.10-1                            amd64        Coin-or linear programming solver (shared libraries)
ii  coinor-libcoinmp1:amd64               1.7.6+dfsg1-1                        amd64        Simple C API for COIN-OR Solvers Clp and Cbc -- library
ii  coinor-libcoinutils3                  2.9.15-3                             amd64        Coin-or collection of utility classes (binaries and libraries)
ii  coinor-libosi1                        0.106.9-1                            amd64        COIN-OR Open Solver Interface
ii  colord                                1.2.1-1+b2                           amd64        system service to manage device colour profiles -- system daemon
ii  colord-data                           1.2.1-1                              all          system service to manage device colour profiles -- data files
ii  coreutils                             8.23-4                               amd64        GNU core utilities
ii  cpio                                  2.11+dfsg-4.1+deb8u1                 amd64        GNU cpio -- a program to manage archives of files
ii  cpp                                   4:4.9.2-2                            amd64        GNU C preprocessor (cpp)
ii  cpp-4.8                               4.8.4-1                              amd64        GNU C preprocessor
ii  cpp-4.9                               4.9.2-10                             amd64        GNU C preprocessor
ii  crda                                  3.13-1                               amd64        wireless Central Regulatory Domain Agent
ii  cron                                  3.0pl1-127+deb8u1                    amd64        process scheduling daemon
ii  cups-client                           1.7.5-11+deb8u1                      amd64        Common UNIX Printing System(tm) - client programs (SysV)
ii  cups-common                           1.7.5-11+deb8u1                      all          Common UNIX Printing System(tm) - common files
ii  cups-pk-helper                        0.2.5-2+b1                           amd64        PolicyKit helper to configure cups with fine-grained privileges
ii  dash                                  0.5.7-4+b1                           amd64        POSIX-compliant shell
ii  dbus                                  1.8.22-0+deb8u1                      amd64        simple interprocess messaging system (daemon and utilities)
ii  dbus-x11                              1.8.22-0+deb8u1                      amd64        simple interprocess messaging system (X11 deps)
ii  dconf-gsettings-backend:amd64         0.22.0-1                             amd64        simple configuration storage system - GSettings back-end
ii  dconf-service                         0.22.0-1                             amd64        simple configuration storage system - D-Bus service
ii  debconf                               1.5.56                               all          Debian configuration management system
ii  debconf-i18n                          1.5.56                               all          full internationalization support for debconf
ii  debian-archive-keyring                2014.3                               all          GnuPG archive keys of the Debian archive
ii  debianutils                           4.4+b1                               amd64        Miscellaneous utilities specific to Debian
ii  desktop-base                          8.0.2                                all          common files for the Debian Desktop
ii  desktop-file-utils                    0.22-1                               amd64        Utilities for .desktop files
ii  dh-python                             1.20141111-2                         all          Debian helper tools for packaging Python libraries and applications
ii  dictionaries-common                   1.23.17                              all          spelling dictionaries - common utilities
ii  diffutils                             1:3.3-1+b1                           amd64        File comparison utilities
ii  dkms                                  2.2.0.3-2                            all          Dynamic Kernel Module Support Framework
ii  dmidecode                             2.12-3                               amd64        SMBIOS/DMI table decoder
ii  dmsetup                               2:1.02.90-2.2+deb8u1                 amd64        Linux Kernel Device Mapper userspace library
ii  dns-root-data                         2014060201+2                         all          DNS root data including root zone and DNSSEC key
ii  dnsmasq-base                          2.72-3+deb8u1                        amd64        Small caching DNS proxy and DHCP/TFTP server
ii  docbook-xml                           4.5-7.2                              all          standard XML documentation system for software and systems
ii  dosfstools                            3.0.27-1                             amd64        utilities for making and checking MS-DOS FAT filesystems
ii  dpkg                                  1.17.27                              amd64        Debian package management system
ii  dpkg-dev                              1.17.27                              all          Debian package development tools
ii  e2fslibs:amd64                        1.42.12-2+b1                         amd64        ext2/ext3/ext4 file system libraries
ii  e2fsprogs                             1.42.12-2+b1                         amd64        ext2/ext3/ext4 file system utilities
ii  eject                                 2.1.5+deb1+cvs20081104-13.1          amd64        ejects CDs and operates CD-Changers under Linux
ii  emacs                                 46.1                                 all          GNU Emacs editor (metapackage)
ii  emacs24                               24.4+1-5                             amd64        GNU Emacs editor (with GTK+ GUI support)
ii  emacs24-bin-common                    24.4+1-5                             amd64        GNU Emacs editor's shared, architecture dependent files
ii  emacs24-common                        24.4+1-5                             all          GNU Emacs editor's shared, architecture independent infrastructure
ii  emacsen-common                        2.0.8                                all          Common facilities for all emacsen
ii  espeak-data:amd64                     1.48.04+dfsg-1                       amd64        Multi-lingual software speech synthesizer: speech data files
ii  evince-common                         3.14.1-2+deb8u1                      all          Document (PostScript, PDF) viewer - common files
ii  evince-gtk                            3.14.1-2+deb8u1                      amd64        Document (PostScript, PDF) viewer (GTK+ version)
ii  exfalso                               3.2.2-1                              all          audio tag editor for GTK+
ii  exo-utils                             0.10.2-4                             amd64        Utility files for libexo
ii  fakeroot                              1.20.2-1                             amd64        tool for simulating superuser privileges
ii  file                                  1:5.22+15-2+deb8u3                   amd64        Determines file type using "magic" numbers
ii  findutils                             4.4.2-9+b1                           amd64        utilities for finding files--find, xargs
ii  firebird2.5-common                    2.5.3.26778.ds4-5                    all          common files for firebird 2.5 servers and clients
ii  firebird2.5-common-doc                2.5.3.26778.ds4-5                    all          copyright, licensing and changelogs of firebird2.5
ii  firebird2.5-server-common             2.5.3.26778.ds4-5                    amd64        common files for firebird 2.5 servers
ii  firefox-esr                           45.7.0esr-1~deb8u1                   amd64        Mozilla Firefox web browser - Extended Support Release (ESR)
ii  firmware-atheros                      0.43                                 all          Binary firmware for Atheros wireless cards
ii  firmware-brcm80211                    0.43                                 all          Binary firmware for Broadcom 802.11 wireless cards
ii  firmware-iwlwifi                      0.43                                 all          Binary firmware for Intel Wireless cards
ii  firmware-libertas                     0.43                                 all          Binary firmware for Marvell Libertas 8xxx wireless cards
ii  firmware-linux-free                   3.3                                  all          Binary firmware for various drivers in the Linux kernel
ii  firmware-ralink                       0.43                                 all          Binary firmware for Ralink wireless cards
ii  firmware-realtek                      0.43                                 all          Binary firmware for Realtek wired and wireless network adapters
ii  fontconfig                            2.11.0-6.3+deb8u1                    amd64        generic font configuration library - support binaries
ii  fontconfig-config                     2.11.0-6.3+deb8u1                    all          generic font configuration library - configuration
ii  fonts-dejavu                          2.34-1                               all          metapackage to pull in fonts-dejavu-core and fonts-dejavu-extra
ii  fonts-dejavu-core                     2.34-1                               all          Vera font family derivate with additional characters
ii  fonts-dejavu-extra                    2.34-1                               all          Vera font family derivate with additional characters (extra variants)
ii  fonts-droid                           1:4.4.4r2-6                          all          handheld device font with extensive style and language support
ii  fonts-freefont-ttf                    20120503-4                           all          Freefont Serif, Sans and Mono Truetype fonts
ii  fonts-liberation                      1.07.4-1                             all          Fonts with the same metrics as Times, Arial and Courier
ii  fonts-opensymbol                      2:102.6+LibO4.3.3-2+deb8u5           all          OpenSymbol TrueType font
ii  fonts-sil-gentium                     20081126:1.02-13                     all          extended Unicode Latin font ("a typeface for the nations")
ii  fonts-sil-gentium-basic               1.1-7                                all          smart Unicode font families (Basic and Book Basic) based on Gentium
ii  fuse                                  2.9.3-15+deb8u2                      amd64        Filesystem in Userspace
ii  g++                                   4:4.9.2-2                            amd64        GNU C++ compiler
ii  g++-4.9                               4.9.2-10                             amd64        GNU C++ compiler
ii  gcc                                   4:4.9.2-2                            amd64        GNU C compiler
ii  gcc-4.8                               4.8.4-1                              amd64        GNU C compiler
ii  gcc-4.8-base:amd64                    4.8.4-1                              amd64        GCC, the GNU Compiler Collection (base package)
ii  gcc-4.9                               4.9.2-10                             amd64        GNU C compiler
ii  gcc-4.9-base:amd64                    4.9.2-10                             amd64        GCC, the GNU Compiler Collection (base package)
ii  gcc-4.9-base:i386                     4.9.2-10                             i386         GCC, the GNU Compiler Collection (base package)
ii  gconf-service                         3.2.6-3                              amd64        GNOME configuration database system (D-Bus service)
ii  gconf2                                3.2.6-3                              amd64        GNOME configuration database system (support tools)
ii  gconf2-common                         3.2.6-3                              all          GNOME configuration database system (common files)
ii  gcr                                   3.14.0-2                             amd64        GNOME crypto services (daemon and tools)
ii  gdebi-core                            0.9.5.5+nmu1                         all          simple tool to install deb files
ii  gdisk                                 0.8.10-2                             amd64        GPT fdisk text-mode partitioning tool
ii  geany                                 1.24.1+dfsg-1                        amd64        fast and lightweight IDE
ii  geany-common                          1.24.1+dfsg-1                        all          fast and lightweight IDE -- common files
ii  geogebra                              4.0.34.0+dfsg1-3                     all          Dynamic mathematics software for education
ii  geoip-database                        20150317-1                           all          IP lookup command line tools that use the GeoIP library (country database)
ii  ghostscript                           9.06~dfsg-2+deb8u4                   amd64        interpreter for the PostScript language and for PDF
ii  gimp                                  2.8.14-1+deb8u1                      amd64        The GNU Image Manipulation Program
ii  gimp-data                             2.8.14-1+deb8u1                      all          Data files for GIMP
ii  gir1.2-atk-1.0                        2.14.0-1                             amd64        ATK accessibility toolkit (GObject introspection)
ii  gir1.2-atspi-2.0                      2.14.0-1                             amd64        Assistive Technology Service Provider (GObject introspection)
ii  gir1.2-freedesktop:amd64              1.42.0-2.2                           amd64        Introspection data for some FreeDesktop components
ii  gir1.2-gdkpixbuf-2.0                  2.31.1-2+deb8u5                      amd64        GDK Pixbuf library - GObject-Introspection
ii  gir1.2-glib-2.0:amd64                 1.42.0-2.2                           amd64        Introspection data for GLib, GObject, Gio and GModule
ii  gir1.2-gnomekeyring-1.0               3.12.0-1+b1                          amd64        GNOME keyring services library - introspection data
ii  gir1.2-gst-plugins-base-1.0           1.4.4-2                              amd64        Description: GObject introspection data for the GStreamer Plugins Base library
ii  gir1.2-gstreamer-1.0                  1.4.4-2                              amd64        Description: GObject introspection data for the GStreamer library
ii  gir1.2-gtk-3.0:amd64                  3.14.5-1+deb8u1                      amd64        GTK+ graphical user interface library -- gir bindings
ii  gir1.2-gtksource-3.0:amd64            3.14.1-1                             amd64        gir files for the GTK+ syntax highlighting widget
ii  gir1.2-keybinder-3.0                  0.3.0-1                              amd64        registers global key bindings for applications - Gtk+3 - typelib
ii  gir1.2-notify-0.7                     0.7.6-2                              amd64        sends desktop notifications to a notification daemon (Introspection files)
ii  gir1.2-packagekitglib-1.0             1.0.1-2                              amd64        GObject introspection data for the PackageKit GLib library
ii  gir1.2-pango-1.0:amd64                1.36.8-3                             amd64        Layout and rendering of internationalized text - gir bindings
ii  gir1.2-wnck-3.0:amd64                 3.4.9-3                              amd64        GObject introspection data for the WNCK library
ii  git                                   1:2.1.4-2.1+deb8u2                   amd64        fast, scalable, distributed revision control system
ii  git-man                               1:2.1.4-2.1+deb8u2                   all          fast, scalable, distributed revision control system (manual pages)
ii  gitk                                  1:2.1.4-2.1+deb8u2                   all          fast, scalable, distributed revision control system (revision tree visualizer)
ii  glib-networking:amd64                 2.42.0-2                             amd64        network-related giomodules for GLib
ii  glib-networking-common                2.42.0-2                             all          network-related giomodules for GLib - data files
ii  glib-networking-services              2.42.0-2                             amd64        network-related giomodules for GLib - D-Bus services
ii  gnome-accessibility-themes            3.14.2.2-1                           all          Accessibility themes for the GNOME desktop
ii  gnome-icon-theme                      3.12.0-1                             all          GNOME Desktop icon theme
ii  gnome-icon-theme-symbolic             3.12.0-1                             all          GNOME desktop icon theme (symbolic icons)
ii  gnome-keyring                         3.14.0-1+b1                          amd64        GNOME keyring services (daemon and tools)
ii  gnome-mime-data                       2.18.0-1                             all          base MIME and Application database for GNOME.
ii  gnome-orca                            3.14.0-4+deb8u1                      all          Scriptable screen reader
ii  gnome-paint                           0.4.0-4                              amd64        simple, easy to use paint program for GNOME
ii  gnome-themes-standard:amd64           3.14.2.2-1                           amd64        Standard GNOME themes
ii  gnome-themes-standard-data            3.14.2.2-1                           all          Data files for GNOME standard themes
ii  gnupg                                 1.4.18-7+deb8u3                      amd64        GNU privacy guard - a free PGP replacement
ii  gpgv                                  1.4.18-7+deb8u3                      amd64        GNU privacy guard - signature verification tool
ii  grep                                  2.20-4.1                             amd64        GNU grep, egrep and fgrep
ii  gringo                                4.4.0-1                              amd64        grounding tools for (disjunctive) logic programs
ii  groff-base                            1.22.2-8                             amd64        GNU troff text-formatting system (base system components)
ii  gsettings-desktop-schemas             3.14.1-1                             all          GSettings desktop-wide schemas
ii  gsfonts                               1:8.11+urwcyr1.0.7~pre44-4.2         all          Fonts for the Ghostscript interpreter(s)
ii  gstreamer0.10-alsa:amd64              0.10.36-2                            amd64        GStreamer plugin for ALSA
ii  gstreamer0.10-plugins-base:amd64      0.10.36-2                            amd64        GStreamer plugins from the "base" set
ii  gstreamer1.0-libav:amd64              1.4.4-2                              amd64        libav plugin for GStreamer
ii  gstreamer1.0-plugins-base:amd64       1.4.4-2                              amd64        GStreamer plugins from the "base" set
ii  gstreamer1.0-plugins-good:amd64       1.4.4-2+deb8u2                       amd64        GStreamer plugins from the "good" set
ii  gstreamer1.0-plugins-ugly:amd64       1.4.4-2+b1                           amd64        GStreamer plugins from the "ugly" set
ii  gstreamer1.0-x:amd64                  1.4.4-2                              amd64        GStreamer plugins for X11 and Pango
ii  gtk2-engines-pixbuf:amd64             2.24.25-3+deb8u1                     amd64        pixbuf-based theme for GTK+ 2.x
ii  gtk2-engines-xfce                     3.0.1-2                              amd64        GTK+-2.0 theme engine for Xfce
ii  gvfs:amd64                            1.22.2-1                             amd64        userspace virtual filesystem - GIO module
ii  gvfs-common                           1.22.2-1                             all          userspace virtual filesystem - common data files
ii  gvfs-daemons                          1.22.2-1                             amd64        userspace virtual filesystem - servers
ii  gvfs-libs:amd64                       1.22.2-1                             amd64        userspace virtual filesystem - private libraries
ii  gzip                                  1.6-4                                amd64        GNU compression utilities
ii  hddtemp                               0.3-beta15-52                        amd64        hard drive temperature monitoring utility
ii  hicolor-icon-theme                    0.13-1                               all          default fallback theme for FreeDesktop.org icon themes
ii  hostname                              3.15                                 amd64        utility to set/show the host name or domain name
ii  hunspell-en-us                        20070829-6                           all          English_american dictionary for hunspell
ii  hyphen-en-us                          2.8.8-1                              all          US English hyphenation patterns for LibreOffice/OpenOffice.org
ii  i965-va-driver:amd64                  1.4.1-2                              amd64        VAAPI driver for Intel G45 & HD Graphics family
ii  icedtea-netx-common                   1.5.3-1                              all          NetX - implementation of the Java Network Launching Protocol (JNLP)
ii  iceweasel                             45.7.0esr-1~deb8u1                   all          Web browser based on Firefox - Transitional package
ii  ifupdown                              0.7.53.1                             amd64        high level tools to configure network interfaces
ii  imagemagick-common                    8:6.8.9.9-5+deb8u6                   all          image manipulation programs -- infrastructure
ii  init                                  1.22                                 amd64        System-V-like init utilities - metapackage
ii  init-system-helpers                   1.22                                 all          helper tools for all init systems
ii  initramfs-tools                       0.120+deb8u2                         all          generic modular initramfs generator
ii  initscripts                           2.88dsf-59                           amd64        scripts for initializing and shutting down the system
ii  insserv                               1.14.0-5                             amd64        boot sequence organizer using LSB init.d script dependency information
ii  iproute2                              3.16.0-2                             amd64        networking and traffic control tools
ii  iptables                              1.4.21-2+b1                          amd64        administration tools for packet filtering and NAT
ii  iputils-arping                        3:20121221-5+b2                      amd64        Tool to send ICMP echo requests to an ARP address
ii  iputils-ping                          3:20121221-5+b2                      amd64        Tools to test the reachability of network hosts
ii  irqbalance                            1.0.6-3                              amd64        Daemon to balance interrupts for SMP systems
ii  isc-dhcp-client                       4.3.1-6+deb8u2                       amd64        DHCP client for automatically obtaining an IP address
ii  isc-dhcp-common                       4.3.1-6+deb8u2                       amd64        common files used by all of the isc-dhcp packages
ii  iso-codes                             3.57-1                               all          ISO language, territory, currency, script codes and their translations
ii  iw                                    3.17-1                               amd64        tool for configuring Linux wireless devices
ii  java-common                           0.52                                 all          Base of all Java packages
ii  javahelp2                             2.0.05.ds1-7                         all          Java based help system
ii  javascript-common                     11                                   all          Base support for JavaScript library packages
ii  junit                                 3.8.2-8                              all          Automated testing framework for Java
ii  junit4                                4.11-3                               all          JUnit regression test framework for Java
ii  keyboard-configuration                1.123                                all          system-wide keyboard preferences
ii  klibc-utils                           2.0.4-2                              amd64        small utilities built with klibc for early boot
ii  kmod                                  18-3                                 amd64        tools for managing Linux kernel modules
ii  krb5-locales                          1.12.1+dfsg-19+deb8u2                all          Internationalization support for MIT Kerberos
ii  less                                  458-3                                amd64        pager program similar to more
ii  liba52-0.7.4                          0.7.4-17                             amd64        library for decoding ATSC A/52 streams
ii  libaa1:amd64                          1.4p5-43                             amd64        ASCII art library
ii  libaacs0:amd64                        0.7.1-1+b1                           amd64        free-and-libre implementation of AACS
ii  libabw-0.1-1                          0.1.0-2                              amd64        library for reading and writing AbiWord(tm) documents
ii  libacl1:amd64                         2.2.52-2                             amd64        Access control list shared library
ii  libalgorithm-c3-perl                  0.09-1                               all          Perl module for merging hierarchies using the C3 algorithm
ii  libalgorithm-diff-perl                1.19.02-3                            all          module to find differences between files
ii  libalgorithm-diff-xs-perl             0.04-3+b1                            amd64        module to find differences between files (XS accelerated)
ii  libalgorithm-merge-perl               0.08-2                               all          Perl module for three-way merge of textual data
ii  libamd2.3.1:amd64                     1:4.2.1-3                            amd64        approximate minimum degree ordering library for sparse matrices
ii  libao-common                          1.1.0-3                              amd64        Cross Platform Audio Output Library (Common files)
ii  libao4                                1.1.0-3                              amd64        Cross Platform Audio Output Library
ii  libapt-inst1.5:amd64                  1.0.9.8.4                            amd64        deb package format runtime library
ii  libapt-pkg4.12:amd64                  1.0.9.8.4                            amd64        package management runtime library
ii  libarchive-extract-perl               0.72-1                               all          generic archive extracting module
ii  libarchive13:amd64                    3.1.2-11+deb8u3                      amd64        Multi-format archive and compression library (shared library)
ii  libasan0:amd64                        4.8.4-1                              amd64        AddressSanitizer -- a fast memory error detector
ii  libasan1:amd64                        4.9.2-10                             amd64        AddressSanitizer -- a fast memory error detector
ii  libasound2:amd64                      1.0.28-1                             amd64        shared library for ALSA applications
ii  libasound2-data                       1.0.28-1                             all          Configuration files and profiles for ALSA drivers
ii  libasound2-plugins:amd64              1.0.28-1+b1                          amd64        ALSA library additional plugins
ii  libaspell15:amd64                     0.60.7~20110707-1.3                  amd64        GNU Aspell spell-checker runtime library
ii  libass5:amd64                         0.10.2-3                             amd64        library for SSA/ASS subtitles rendering
ii  libasyncns0:amd64                     0.8-5                                amd64        Asynchronous name service query library
ii  libatasmart4:amd64                    0.19-3                               amd64        ATA S.M.A.R.T. reading and parsing library
ii  libatk-adaptor:amd64                  2.14.0-2                             amd64        AT-SPI 2 toolkit bridge
ii  libatk-bridge2.0-0:amd64              2.14.0-2                             amd64        AT-SPI 2 toolkit bridge - shared library
ii  libatk-wrapper-java                   0.30.5-1                             all          ATK implementation for Java using JNI
ii  libatk-wrapper-java-jni:amd64         0.30.5-1                             amd64        ATK implementation for Java using JNI (JNI bindings)
ii  libatk1.0-0:amd64                     2.14.0-1                             amd64        ATK accessibility toolkit
ii  libatk1.0-0:i386                      2.14.0-1                             i386         ATK accessibility toolkit
ii  libatk1.0-data                        2.14.0-1                             all          Common files for the ATK accessibility toolkit
ii  libatomic1:amd64                      4.9.2-10                             amd64        support library providing __atomic built-in functions
ii  libatspi2.0-0:amd64                   2.14.0-1                             amd64        Assistive Technology Service Provider Interface - shared library
ii  libattr1:amd64                        1:2.4.47-2                           amd64        Extended attribute shared library
ii  libaudio2:amd64                       1.9.4-3                              amd64        Network Audio System - shared libraries
ii  libaudit-common                       1:2.4-1                              all          Dynamic library for security auditing - common files
ii  libaudit1:amd64                       1:2.4-1+b1                           amd64        Dynamic library for security auditing
ii  libauthen-sasl-perl                   2.1600-1                             all          Authen::SASL - SASL Authentication framework
ii  libavahi-client3:amd64                0.6.31-5                             amd64        Avahi client library
ii  libavahi-client3:i386                 0.6.31-5                             i386         Avahi client library
ii  libavahi-common-data:amd64            0.6.31-5                             amd64        Avahi common data files
ii  libavahi-common-data:i386             0.6.31-5                             i386         Avahi common data files
ii  libavahi-common3:amd64                0.6.31-5                             amd64        Avahi common library
ii  libavahi-common3:i386                 0.6.31-5                             i386         Avahi common library
ii  libavahi-core7:amd64                  0.6.31-5                             amd64        Avahi's embeddable mDNS/DNS-SD library
ii  libavahi-glib1:amd64                  0.6.31-5                             amd64        Avahi GLib integration library
ii  libavc1394-0:amd64                    0.5.4-2                              amd64        control IEEE 1394 audio/video devices
ii  libavcodec56:amd64                    6:11.8-1~deb8u1                      amd64        Libav codec library
ii  libavformat56:amd64                   6:11.8-1~deb8u1                      amd64        Libav file format library
ii  libavresample2:amd64                  6:11.8-1~deb8u1                      amd64        Libav audio resampling library
ii  libavutil54:amd64                     6:11.8-1~deb8u1                      amd64        Libav utility library
ii  libbabl-0.1-0:amd64                   0.1.10-2                             amd64        Dynamic, any to any, pixel format conversion library
ii  libbasicusageenvironment0             2014.01.13-1                         amd64        multimedia RTSP streaming library (BasicUsageEnvironment class)
ii  libbind9-90                           1:9.9.5.dfsg-9+deb8u9                amd64        BIND9 Shared Library used by BIND
ii  libblas-common                        1.2.20110419-10                      amd64        Dependency package for all BLAS implementations
ii  libblas3                              1.2.20110419-10                      amd64        Basic Linear Algebra Reference implementations, shared library
ii  libblkid1:amd64                       2.25.2-6                             amd64        block device id library
ii  libbluetooth3:amd64                   5.23-2+b1                            amd64        Library to use the BlueZ Linux Bluetooth stack
ii  libbluray1:amd64                      1:0.6.2-1                            amd64        Blu-ray disc playback support library (shared library)
ii  libbonobo2-0:amd64                    2.32.1-3                             amd64        Bonobo CORBA interfaces library
ii  libbonobo2-common                     2.32.1-3                             all          Bonobo CORBA interfaces library -- support files
ii  libboost-date-time1.55.0:amd64        1.55.0+dfsg-3                        amd64        set of date-time libraries based on generic programming concepts
ii  libboost-iostreams1.55.0:amd64        1.55.0+dfsg-3                        amd64        Boost.Iostreams Library
ii  libboost-system1.55.0:amd64           1.55.0+dfsg-3                        amd64        Operating system (e.g. diagnostics support) library
ii  libbrlapi0.6:amd64                    5.2~20141018-5                       amd64        braille display access via BRLTTY - shared library
ii  libbsd0:amd64                         0.7.0-2                              amd64        utility functions from BSD systems - shared library
ii  libburn4                              1.3.2-1.1                            amd64        library to provide CD/DVD writing functions
ii  libbz2-1.0:amd64                      1.0.6-7+b3                           amd64        high-quality block-sorting file compressor library - runtime
ii  libbz2-1.0:i386                       1.0.6-7+b3                           i386         high-quality block-sorting file compressor library - runtime
ii  libc-bin                              2.19-18+deb8u7                       amd64        GNU C Library: Binaries
ii  libc-dev-bin                          2.19-18+deb8u7                       amd64        GNU C Library: Development binaries
ii  libc6:amd64                           2.19-18+deb8u7                       amd64        GNU C Library: Shared libraries
ii  libc6:i386                            2.19-18+deb8u7                       i386         GNU C Library: Shared libraries
ii  libc6-dev:amd64                       2.19-18+deb8u7                       amd64        GNU C Library: Development Libraries and Header Files
ii  libc6-i686:i386                       2.19-18+deb8u7                       i386         GNU C Library: Shared libraries [i686 optimized]
ii  libcaca0:amd64                        0.99.beta19-2                        amd64        colour ASCII art library
ii  libcairo-gobject2:amd64               1.14.0-2.1+deb8u2                    amd64        Cairo 2D vector graphics library (GObject library)
ii  libcairo-perl                         1.104-2                              amd64        Perl interface to the Cairo graphics library
ii  libcairo2:amd64                       1.14.0-2.1+deb8u2                    amd64        Cairo 2D vector graphics library
ii  libcairo2:i386                        1.14.0-2.1+deb8u2                    i386         Cairo 2D vector graphics library
ii  libcamd2.3.1:amd64                    1:4.2.1-3                            amd64        symmetric approximate minimum degree library for sparse matrices
ii  libcanberra-gtk3-0:amd64              0.30-2.1                             amd64        GTK+ 3.0 helper for playing widget event sounds with libcanberra
ii  libcanberra-gtk3-module:amd64         0.30-2.1                             amd64        translates GTK3 widgets signals to event sounds
ii  libcanberra0:amd64                    0.30-2.1                             amd64        simple abstract interface for playing event sounds
ii  libcap-ng0:amd64                      0.7.4-2                              amd64        An alternate POSIX capabilities library
ii  libcap2:amd64                         1:2.24-8                             amd64        POSIX 1003.1e capabilities (library)
ii  libcap2-bin                           1:2.24-8                             amd64        POSIX 1003.1e capabilities (utilities)
ii  libccolamd2.8.0:amd64                 1:4.2.1-3                            amd64        constrained column approximate library for sparse matrices
ii  libcddb2                              1.3.2-5                              amd64        library to access CDDB data - runtime files
ii  libcdio13                             0.83-4.2                             amd64        library to read and control CD-ROM
ii  libcdparanoia0:amd64                  3.10.2+debian-11                     amd64        audio extraction tool for sampling CDs (library)
ii  libcdr-0.1-1                          0.1.0-3                              amd64        library for reading and converting Corel DRAW files
ii  libcgi-fast-perl                      1:2.04-1                             all          CGI subclass for work with FCGI
ii  libcgi-pm-perl                        4.09-1                               all          module for Common Gateway Interface applications
ii  libcholmod2.1.2:amd64                 1:4.2.1-3                            amd64        sparse Cholesky factorization library for sparse matrices
ii  libchromaprint0:amd64                 1.2-1                                amd64        audio fingerprint library
ii  libcilkrts5:amd64                     4.9.2-10                             amd64        Intel Cilk Plus language extensions (runtime)
ii  libclass-c3-perl                      0.26-1                               all          pragma for using the C3 method resolution order
ii  libclass-c3-xs-perl                   0.13-2+b1                            amd64        Perl module to accelerate Class::C3
ii  libcloog-isl4:amd64                   0.18.2-1+b2                          amd64        Chunky Loop Generator (runtime library)
ii  libclucene-contribs1:amd64            2.3.3.4-4                            amd64        language specific text analyzers (runtime)
ii  libclucene-core1:amd64                2.3.3.4-4                            amd64        core library for full-featured text search engine (runtime)
ii  libcmis-0.4-4                         0.4.1-7                              amd64        CMIS protocol client library
ii  libcolamd2.8.0:amd64                  1:4.2.1-3                            amd64        column approximate minimum degree ordering library for sparse matrices
ii  libcolord2:amd64                      1.2.1-1+b2                           amd64        system service to manage device colour profiles -- runtime
ii  libcolorhug2:amd64                    1.2.1-1+b2                           amd64        library to access the ColorHug colourimeter -- runtime
ii  libcomerr2:amd64                      1.42.12-2+b1                         amd64        common error description library
ii  libcomerr2:i386                       1.42.12-2+b1                         i386         common error description library
ii  libcommons-collections3-java          3.2.1-7+deb8u1                       all          Apache Commons Collections - Extended Collections API for Java
ii  libcommons-math-java                  2.2-4                                all          Java lightweight mathematics and statistics components
ii  libcpan-meta-perl                     2.142690-1                           all          Perl module to access CPAN distributions metadata
ii  libcroco3:amd64                       0.6.8-3+b1                           amd64        Cascading Style Sheet (CSS) parsing and manipulation toolkit
ii  libcryptsetup4:amd64                  2:1.6.6-5                            amd64        disk encryption support - shared library
ii  libcrystalhd3:amd64                   1:0.0~git20110715.fdd2f19-11         amd64        Crystal HD Video Decoder (shared library)
ii  libcups2:amd64                        1.7.5-11+deb8u1                      amd64        Common UNIX Printing System(tm) - Core library
ii  libcups2:i386                         1.7.5-11+deb8u1                      i386         Common UNIX Printing System(tm) - Core library
ii  libcupsfilters1:amd64                 1.0.61-5+deb8u3                      amd64        OpenPrinting CUPS Filters - Shared library
ii  libcupsimage2:amd64                   1.7.5-11+deb8u1                      amd64        Common UNIX Printing System(tm) - Raster image library
ii  libcurl3-gnutls:amd64                 7.38.0-4+deb8u5                      amd64        easy-to-use client-side URL transfer library (GnuTLS flavour)
ii  libdaemon0:amd64                      0.14-6                               amd64        lightweight C library for daemons - runtime library
ii  libdata-optlist-perl                  0.109-1                              all          module to parse and validate simple name/value option pairs
ii  libdata-section-perl                  0.200006-1                           all          module to read chunks of data from a module's DATA section
ii  libdatrie1:amd64                      0.2.8-1                              amd64        Double-array trie library
ii  libdatrie1:i386                       0.2.8-1                              i386         Double-array trie library
ii  libdb5.3:amd64                        5.3.28-9                             amd64        Berkeley v5.3 Database Libraries [runtime]
ii  libdbus-1-3:amd64                     1.8.22-0+deb8u1                      amd64        simple interprocess messaging system (library)
ii  libdbus-1-3:i386                      1.8.22-0+deb8u1                      i386         simple interprocess messaging system (library)
ii  libdbus-glib-1-2:amd64                0.102-1                              amd64        simple interprocess messaging system (GLib-based shared library)
ii  libdc1394-22:amd64                    2.2.3-1                              amd64        high level programming interface for IEEE1394 digital camera
ii  libdca0:amd64                         0.0.5-7                              amd64        decoding library for DTS Coherent Acoustics streams
ii  libdconf1:amd64                       0.22.0-1                             amd64        simple configuration storage system - runtime library
ii  libdebconfclient0:amd64               0.192                                amd64        Debian Configuration Management System (C-implementation library)
ii  libdevmapper1.02.1:amd64              2:1.02.90-2.2+deb8u1                 amd64        Linux Kernel Device Mapper userspace library
ii  libdirectfb-1.2-9:amd64               1.2.10.0-5.1                         amd64        direct frame buffer graphics - shared libraries
ii  libdiscid0:amd64                      0.6.1-3                              amd64        library for creating MusicBrainz DiscIDs
ii  libdjvulibre-text                     3.5.25.4-4                           all          Linguistic support files for libdjvulibre
ii  libdjvulibre21:amd64                  3.5.25.4-4+b1                        amd64        Runtime support for the DjVu image format
ii  libdns-export100                      1:9.9.5.dfsg-9+deb8u9                amd64        Exported DNS Shared Library
ii  libdns100                             1:9.9.5.dfsg-9+deb8u9                amd64        DNS Shared Library used by BIND
ii  libdom4j-java                         1.6.1+dfsg.3-2                       all          flexible XML framework for Java
ii  libdotconf0:amd64                     1.3-0.2                              amd64        Configuration file parser library - runtime files
ii  libdpkg-perl                          1.17.27                              all          Dpkg perl modules
ii  libdrm-intel1:amd64                   2.4.58-2                             amd64        Userspace interface to intel-specific kernel DRM services -- runtime
ii  libdrm-nouveau2:amd64                 2.4.58-2                             amd64        Userspace interface to nouveau-specific kernel DRM services -- runtime
ii  libdrm-radeon1:amd64                  2.4.58-2                             amd64        Userspace interface to radeon-specific kernel DRM services -- runtime
ii  libdrm2:amd64                         2.4.58-2                             amd64        Userspace interface to kernel DRM services -- runtime
ii  libdv4:amd64                          1.0.0-6                              amd64        software library for DV format digital video (runtime lib)
ii  libdvbpsi9:amd64                      1.2.0-1                              amd64        library for MPEG TS and DVB PSI tables decoding and generating
ii  libdvdnav4:amd64                      5.0.1-1                              amd64        DVD navigation library
ii  libdvdread4:amd64                     5.0.0-1                              amd64        library for reading DVDs
ii  libe-book-0.1-1                       0.1.1-2                              amd64        library for reading and converting various  e-book formats
ii  libebml4:amd64                        1.3.0-2+deb8u1                       amd64        access library for the EBML format (shared library)
ii  libedit2:amd64                        3.1-20140620-2                       amd64        BSD editline and history libraries
ii  libegl1-mesa:amd64                    10.3.2-1+deb8u1                      amd64        free implementation of the EGL API -- runtime
ii  libegl1-mesa-drivers:amd64            10.3.2-1+deb8u1                      amd64        free implementation of the EGL API -- hardware drivers
ii  libelf1:amd64                         0.159-4.2                            amd64        library to read and write ELF files
ii  libelfg0:amd64                        0.8.13-5                             amd64        an ELF object file access library
ii  libenca0:amd64                        1.16-1                               amd64        Extremely Naive Charset Analyser - shared library files
ii  libencode-locale-perl                 1.03-1                               all          utility to determine the locale encoding
ii  libeot0                               0.01-3                               amd64        Library for parsing/converting Embedded OpenType files
ii  libepoxy0                             1.2-1                                amd64        OpenGL function pointer management library
ii  libept1.4.12:amd64                    1.0.12.1                             amd64        High-level library for managing Debian package information
ii  liberror-perl                         0.17-1.1                             all          Perl module for error/exception handling in an OO-ish way
ii  libespeak1:amd64                      1.48.04+dfsg-1                       amd64        Multi-lingual software speech synthesizer: shared library
ii  libestr0                              0.1.9-1.1                            amd64        Helper functions for handling strings (lib)
ii  libetonyek-0.1-1                      0.1.1-2                              amd64        library for reading and converting Apple Keynote presentations
ii  libevdev2                             1.3+dfsg-1                           amd64        wrapper library for evdev devices
ii  libevdocument3-4                      3.14.1-2+deb8u1                      amd64        Document (PostScript, PDF) rendering library
ii  libevent-2.0-5:amd64                  2.0.21-stable-2+deb8u1               amd64        Asynchronous event notification library
ii  libevview3-3                          3.14.1-2+deb8u1                      amd64        Document (PostScript, PDF) rendering library - Gtk+ widgets
ii  libexif12:amd64                       0.6.21-2                             amd64        library to parse EXIF files
ii  libexo-1-0:amd64                      0.10.2-4                             amd64        Library with extensions for Xfce
ii  libexo-common                         0.10.2-4                             all          libexo common files
ii  libexo-helpers                        0.10.2-4                             amd64        helpers for the exo library
ii  libexpat1:amd64                       2.1.0-6+deb8u3                       amd64        XML parsing C library - runtime library
ii  libexpat1:i386                        2.1.0-6+deb8u3                       i386         XML parsing C library - runtime library
ii  libexttextcat-2.0-0                   3.4.4-1                              amd64        Language detection library
ii  libexttextcat-data                    3.4.4-1                              all          Language detection library - data files
ii  libfaad2:amd64                        2.7-8                                amd64        freeware Advanced Audio Decoder - runtime files
ii  libfakeroot:amd64                     1.20.2-1                             amd64        tool for simulating superuser privileges - shared libraries
ii  libfbclient2:amd64                    2.5.3.26778.ds4-5                    amd64        Firebird client library
ii  libfbembed2.5                         2.5.3.26778.ds4-5                    amd64        Firebird embedded client/server library
ii  libfcgi-perl                          0.77-1+deb8u1                        amd64        helper module for FastCGI
ii  libffi6:amd64                         3.1-2+b2                             amd64        Foreign Function Interface library runtime
ii  libffi6:i386                          3.1-2+b2                             i386         Foreign Function Interface library runtime
ii  libfftw3-double3:amd64                3.3.4-2                              amd64        Library for computing Fast Fourier Transforms - Double precision
ii  libfftw3-single3:amd64                3.3.4-2                              amd64        Library for computing Fast Fourier Transforms - Single precision
ii  libfile-basedir-perl                  0.03-1                               all          Perl module to use the freedesktop basedir specification
ii  libfile-copy-recursive-perl           0.38-1                               all          Perl extension for recursively copying files and directories
ii  libfile-desktopentry-perl             0.07-1                               all          Perl module to handle freedesktop .desktop files
ii  libfile-fcntllock-perl                0.22-1+b1                            amd64        Perl module for file locking with fcntl(2)
ii  libfile-listing-perl                  6.04-1                               all          module to parse directory listings
ii  libfile-mimeinfo-perl                 0.26-1                               all          Perl module to determine file types
ii  libflac8:amd64                        1.3.0-3                              amd64        Free Lossless Audio Codec - runtime C library
ii  libflite1:amd64                       1.4-release-12                       amd64        Small run-time speech synthesis engine - shared libraries
ii  libfont-afm-perl                      1.20-1                               all          Font::AFM - Interface to Adobe Font Metrics files
ii  libfontconfig1:amd64                  2.11.0-6.3+deb8u1                    amd64        generic font configuration library - runtime
ii  libfontconfig1:i386                   2.11.0-6.3+deb8u1                    i386         generic font configuration library - runtime
ii  libfontenc1:amd64                     1:1.1.2-1+b2                         amd64        X11 font encoding library
ii  libfreehand-0.1-1                     0.1.0-2                              amd64        Library for parsing the FreeHand file format structure
ii  libfreehep-export-java                2.1.1-2                              all          FreeHEP Export and Save As Library
ii  libfreehep-graphics2d-java            2.1.1-4                              all          FreeHEP 2D Graphics Library
ii  libfreehep-graphicsio-emf-java        2.1.1-emfplus+dfsg1-2                all          FreeHEP Enhanced Metafile Format Driver
ii  libfreehep-graphicsio-java            2.1.1-3                              all          FreeHEP GraphicsIO Base Library
ii  libfreehep-graphicsio-pdf-java        2.1.1+dfsg-1                         all          FreeHEP Portable Document Format Driver
ii  libfreehep-graphicsio-svg-java        2.1.1-3                              all          FreeHEP Scalable Vector Graphics Driver
ii  libfreehep-graphicsio-tests-java      2.1.1+dfsg1-3                        all          FreeHEP GraphicsIO Test Library
ii  libfreehep-io-java                    2.0.2-4                              all          FreeHEP I/O library
ii  libfreehep-swing-java                 2.0.3-3                              all          FreeHEP swing extensions
ii  libfreehep-util-java                  2.0.2-5                              all          FreeHEP utility library
ii  libfreehep-xml-java                   2.1.2+dfsg1-3                        all          FreeHEP XML Library
ii  libfreerdp-cache1.1:amd64             1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Free Remote Desktop Protocol library (cache library)
ii  libfreerdp-client1.1:amd64            1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Free Remote Desktop Protocol library (client library)
ii  libfreerdp-codec1.1:amd64             1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Free Remote Desktop Protocol library (codec library)
ii  libfreerdp-common1.1.0:amd64          1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Free Remote Desktop Protocol library (common library)
ii  libfreerdp-core1.1:amd64              1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Free Remote Desktop Protocol library (core library)
ii  libfreerdp-crypto1.1:amd64            1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Free Remote Desktop Protocol library (freerdp-crypto library)
ii  libfreerdp-gdi1.1:amd64               1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Free Remote Desktop Protocol library (GDI library)
ii  libfreerdp-locale1.1:amd64            1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Free Remote Desktop Protocol library (locale library)
ii  libfreerdp-primitives1.1:amd64        1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Free Remote Desktop Protocol library (primitives library)
ii  libfreerdp-rail1.1:amd64              1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Free Remote Desktop Protocol library (rail library)
ii  libfreerdp-utils1.1:amd64             1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Free Remote Desktop Protocol library (freerdp-utils library)
ii  libfreetype6:amd64                    2.5.2-3+deb8u1                       amd64        FreeType 2 font engine, shared library files
ii  libfreetype6:i386                     2.5.2-3+deb8u1                       i386         FreeType 2 font engine, shared library files
ii  libfribidi0:amd64                     0.19.6-3                             amd64        Free Implementation of the Unicode BiDi algorithm
ii  libfuse2:amd64                        2.9.3-15+deb8u2                      amd64        Filesystem in Userspace (library)
ii  libgail-common:amd64                  2.24.25-3+deb8u1                     amd64        GNOME Accessibility Implementation Library -- common modules
ii  libgail18:amd64                       2.24.25-3+deb8u1                     amd64        GNOME Accessibility Implementation Library -- shared libraries
ii  libgarcon-1-0                         0.2.1-2                              amd64        freedesktop.org compliant menu implementation for Xfce
ii  libgarcon-common                      0.2.1-2                              all          common files for libgarcon menu implementation
ii  libgbm1:amd64                         10.3.2-1+deb8u1                      amd64        generic buffer management API -- runtime
ii  libgcc-4.8-dev:amd64                  4.8.4-1                              amd64        GCC support library (development files)
ii  libgcc-4.9-dev:amd64                  4.9.2-10                             amd64        GCC support library (development files)
ii  libgcc1:amd64                         1:4.9.2-10                           amd64        GCC support library
ii  libgcc1:i386                          1:4.9.2-10                           i386         GCC support library
ii  libgck-1-0:amd64                      3.14.0-2                             amd64        Glib wrapper library for PKCS#11 - runtime
ii  libgconf-2-4:amd64                    3.2.6-3                              amd64        GNOME configuration database system (shared libraries)
ii  libgconf2-4:amd64                     3.2.6-3                              amd64        GNOME configuration database system (dummy package)
ii  libgcr-3-common                       3.14.0-2                             all          Library for Crypto UI related tasks - common files
ii  libgcr-base-3-1:amd64                 3.14.0-2                             amd64        Library for Crypto related tasks
ii  libgcr-ui-3-1:amd64                   3.14.0-2                             amd64        Library for Crypto UI related tasks
ii  libgcrypt20:amd64                     1.6.3-2+deb8u2                       amd64        LGPL Crypto library - runtime library
ii  libgcrypt20:i386                      1.6.3-2+deb8u2                       i386         LGPL Crypto library - runtime library
ii  libgd3:amd64                          2.1.0-5+deb8u9                       amd64        GD Graphics Library
ii  libgdbm3:amd64                        1.8.3-13.1                           amd64        GNU dbm database routines (runtime version)
ii  libgdk-pixbuf2.0-0:amd64              2.31.1-2+deb8u5                      amd64        GDK Pixbuf library
ii  libgdk-pixbuf2.0-0:i386               2.31.1-2+deb8u5                      i386         GDK Pixbuf library
ii  libgdk-pixbuf2.0-common               2.31.1-2+deb8u5                      all          GDK Pixbuf library - data files
ii  libgegl-0.2-0:amd64                   0.2.0-7+b1                           amd64        Generic Graphics Library
ii  libgeoip1:amd64                       1.6.2-4                              amd64        non-DNS IP-to-country resolver library
ii  libgfortran3:amd64                    4.9.2-10                             amd64        Runtime library for GNU Fortran applications
ii  libgif4:amd64                         4.1.6-11+deb8u1                      amd64        library for GIF images (library)
ii  libgimp2.0                            2.8.14-1+deb8u1                      amd64        Libraries for the GNU Image Manipulation Program
ii  libgirepository-1.0-1:amd64           1.42.0-2.2                           amd64        Library for handling GObject introspection data (runtime library)
ii  libgl1-mesa-dri:amd64                 10.3.2-1+deb8u1                      amd64        free implementation of the OpenGL API -- DRI modules
ii  libgl1-mesa-glx:amd64                 10.3.2-1+deb8u1                      amd64        free implementation of the OpenGL API -- GLX runtime
ii  libglade2-0:amd64                     1:2.6.4-2                            amd64        library to load .glade files at runtime
ii  libglapi-mesa:amd64                   10.3.2-1+deb8u1                      amd64        free implementation of the GL API -- shared library
ii  libgles1-mesa:amd64                   10.3.2-1+deb8u1                      amd64        free implementation of the OpenGL|ES 1.x API -- runtime
ii  libgles2-mesa:amd64                   10.3.2-1+deb8u1                      amd64        free implementation of the OpenGL|ES 2.x API -- runtime
ii  libglew1.10:amd64                     1.10.0-3                             amd64        OpenGL Extension Wrangler - runtime environment
ii  libglib-perl                          3:1.305-2                            amd64        interface to the GLib and GObject libraries
ii  libglib2.0-0:amd64                    2.42.1-1+b1                          amd64        GLib library of C routines
ii  libglib2.0-0:i386                     2.42.1-1+b1                          i386         GLib library of C routines
ii  libglib2.0-bin                        2.42.1-1+b1                          amd64        Programs for the GLib library
ii  libglib2.0-data                       2.42.1-1                             all          Common files for GLib library
ii  libgltf-0.0-0                         0.0.2-2                              amd64        Library for rendering glTF models
ii  libglu1-mesa:amd64                    9.0.0-2                              amd64        Mesa OpenGL utility library (GLU)
ii  libgmp10:amd64                        2:6.0.0+dfsg-6                       amd64        Multiprecision arithmetic library
ii  libgmp10:i386                         2:6.0.0+dfsg-6                       i386         Multiprecision arithmetic library
ii  libgnome-2-0:amd64                    2.32.1-5                             amd64        The GNOME library - runtime files
ii  libgnome-keyring-common               3.12.0-1                             all          GNOME keyring services library - data files
ii  libgnome-keyring0:amd64               3.12.0-1+b1                          amd64        GNOME keyring services library
ii  libgnome2-0:amd64                     2.32.1-5                             amd64        The GNOME library - transition package
ii  libgnome2-bin                         2.32.1-5                             amd64        The GNOME library - binary files
ii  libgnome2-common                      2.32.1-5                             all          The GNOME library - common files
ii  libgnomevfs2-0:amd64                  1:2.24.4-6+b1                        amd64        GNOME Virtual File System (runtime libraries)
ii  libgnomevfs2-common                   1:2.24.4-6                           all          GNOME Virtual File System (common files)
ii  libgnomevfs2-extra:amd64              1:2.24.4-6+b1                        amd64        GNOME Virtual File System (extra modules)
ii  libgnutls-deb0-28:amd64               3.3.8-6+deb8u4                       amd64        GNU TLS library - main runtime library
ii  libgnutls-deb0-28:i386                3.3.8-6+deb8u4                       i386         GNU TLS library - main runtime library
ii  libgnutls-openssl27:amd64             3.3.8-6+deb8u4                       amd64        GNU TLS library - OpenSSL wrapper
ii  libgomp1:amd64                        4.9.2-10                             amd64        GCC OpenMP (GOMP) support library
ii  libgpg-error0:amd64                   1.17-3                               amd64        library for common error values and messages in GnuPG components
ii  libgpg-error0:i386                    1.17-3                               i386         library for common error values and messages in GnuPG components
ii  libgphoto2-6:amd64                    2.5.4-1.1+b2                         amd64        gphoto2 digital camera library
ii  libgphoto2-l10n                       2.5.4-1.1                            all          gphoto2 digital camera library - localized messages
ii  libgphoto2-port10:amd64               2.5.4-1.1+b2                         amd64        gphoto2 digital camera port library
ii  libgpm2:amd64                         1.20.4-6.1+b2                        amd64        General Purpose Mouse - shared library
ii  libgpod-common                        0.8.3-1.1+b1                         amd64        common files for libgpod
ii  libgpod4:amd64                        0.8.3-1.1+b1                         amd64        library to read and write songs and artwork to an iPod
ii  libgraphite2-3:amd64                  1.3.6-1~deb8u1                       amd64        Font rendering engine for Complex Scripts -- library
ii  libgraphite2-3:i386                   1.3.6-1~deb8u1                       i386         Font rendering engine for Complex Scripts -- library
ii  libgroupsock1                         2014.01.13-1                         amd64        multimedia RTSP streaming library (network interfaces and sockets)
ii  libgs9                                9.06~dfsg-2+deb8u4                   amd64        interpreter for the PostScript language and for PDF - Library
ii  libgs9-common                         9.06~dfsg-2+deb8u4                   all          interpreter for the PostScript language and for PDF - common files
ii  libgsm1:amd64                         1.0.13-4                             amd64        Shared libraries for GSM speech compressor
ii  libgssapi-krb5-2:amd64                1.12.1+dfsg-19+deb8u2                amd64        MIT Kerberos runtime libraries - krb5 GSS-API Mechanism
ii  libgssapi-krb5-2:i386                 1.12.1+dfsg-19+deb8u2                i386         MIT Kerberos runtime libraries - krb5 GSS-API Mechanism
ii  libgstreamer-plugins-base0.10-0:amd64 0.10.36-2                            amd64        GStreamer libraries from the "base" set
ii  libgstreamer-plugins-base1.0-0:amd64  1.4.4-2                              amd64        GStreamer libraries from the "base" set
ii  libgstreamer0.10-0:amd64              0.10.36-1.5                          amd64        Core GStreamer libraries and elements
ii  libgstreamer1.0-0:amd64               1.4.4-2                              amd64        Core GStreamer libraries and elements
ii  libgtk-3-0:amd64                      3.14.5-1+deb8u1                      amd64        GTK+ graphical user interface library
ii  libgtk-3-bin                          3.14.5-1+deb8u1                      amd64        programs for the GTK+ graphical user interface library
ii  libgtk-3-common                       3.14.5-1+deb8u1                      all          common files for the GTK+ graphical user interface library
ii  libgtk2-perl                          2:1.2492-4                           amd64        Perl interface to the 2.x series of the Gimp Toolkit library
ii  libgtk2.0-0:amd64                     2.24.25-3+deb8u1                     amd64        GTK+ graphical user interface library
ii  libgtk2.0-0:i386                      2.24.25-3+deb8u1                     i386         GTK+ graphical user interface library
ii  libgtk2.0-bin                         2.24.25-3+deb8u1                     amd64        programs for the GTK+ graphical user interface library
ii  libgtk2.0-common                      2.24.25-3+deb8u1                     all          common files for the GTK+ graphical user interface library
ii  libgtksourceview-3.0-1:amd64          3.14.1-1                             amd64        shared libraries for the GTK+ syntax highlighting widget
ii  libgtksourceview-3.0-common           3.14.1-1                             all          common files for the GTK+ syntax highlighting widget
ii  libgtksourceview2.0-0                 2.10.5-2                             amd64        shared libraries for the GTK+ syntax highlighting widget
ii  libgtksourceview2.0-common            2.10.5-2                             all          common files for the GTK+ syntax highlighting widget
ii  libgudev-1.0-0:amd64                  215-17+deb8u6                        amd64        GObject-based wrapper library for libudev
ii  libgusb2:amd64                        0.1.6-5                              amd64        GLib wrapper around libusb1
ii  libgxps2:amd64                        0.2.2-3+b2                           amd64        handling and rendering XPS documents (library)
ii  libhamcrest-java                      1.3-5                                all          library of matchers for building test expressions
ii  libharfbuzz-icu0:amd64                0.9.35-2                             amd64        OpenType text shaping engine ICU backend
ii  libharfbuzz0b:amd64                   0.9.35-2                             amd64        OpenType text shaping engine (shared library)
ii  libharfbuzz0b:i386                    0.9.35-2                             i386         OpenType text shaping engine (shared library)
ii  libhogweed2:amd64                     2.7.1-5+deb8u2                       amd64        low level cryptographic library (public-key cryptos)
ii  libhogweed2:i386                      2.7.1-5+deb8u2                       i386         low level cryptographic library (public-key cryptos)
ii  libhsqldb1.8.0-java                   1.8.0.10+dfsg-3                      all          Java SQL database engine
ii  libhtml-form-perl                     6.03-1                               all          module that represents an HTML form element
ii  libhtml-format-perl                   2.11-1                               all          module for transforming HTML into various formats
ii  libhtml-parser-perl                   3.71-1+b3                            amd64        collection of modules that parse HTML text documents
ii  libhtml-tagset-perl                   3.20-2                               all          Data tables pertaining to HTML
ii  libhtml-tree-perl                     5.03-1                               all          Perl module to represent and create HTML syntax trees
ii  libhttp-cookies-perl                  6.01-1                               all          HTTP cookie jars
ii  libhttp-daemon-perl                   6.01-1                               all          simple http server class
ii  libhttp-date-perl                     6.02-1                               all          module of date conversion routines
ii  libhttp-message-perl                  6.06-1                               all          perl interface to HTTP style messages
ii  libhttp-negotiate-perl                6.00-2                               all          implementation of content negotiation
ii  libhunspell-1.3-0:amd64               1.3.3-3                              amd64        spell checker and morphological analyzer (shared library)
ii  libhyphen0                            2.8.8-1                              amd64        ALTLinux hyphenation library - shared library
ii  libical1a                             1.0-1.3                              amd64        iCalendar library implementation in C (runtime)
ii  libice-dev:amd64                      2:1.0.9-1+b1                         amd64        X11 Inter-Client Exchange library (development headers)
ii  libice6:amd64                         2:1.0.9-1+b1                         amd64        X11 Inter-Client Exchange library
ii  libice6:i386                          2:1.0.9-1+b1                         i386         X11 Inter-Client Exchange library
ii  libicu52:amd64                        52.1-8+deb8u4                        amd64        International Components for Unicode
ii  libidn11:amd64                        1.29-1+deb8u2                        amd64        GNU Libidn library, implementation of IETF IDN specifications
ii  libiec61883-0:amd64                   1.2.0-0.2                            amd64        an partial implementation of IEC 61883
ii  libieee1284-3:amd64                   0.2.11-12                            amd64        cross-platform library for parallel port access
ii  libijs-0.35:amd64                     0.35-10                              amd64        IJS raster image transport protocol: shared library
ii  libilmbase6:amd64                     1.0.1-6.1                            amd64        several utility libraries from ILM used by OpenEXR
ii  libimobiledevice4:amd64               1.1.6+dfsg-3.1                       amd64        Library for communicating with the iPhone and iPod Touch
ii  libio-html-perl                       1.001-1                              all          open an HTML file with automatic charset detection
ii  libio-socket-ssl-perl                 2.002-2+deb8u2                       all          Perl module implementing object oriented interface to SSL sockets
ii  libirs-export91                       1:9.9.5.dfsg-9+deb8u9                amd64        Exported IRS Shared Library
ii  libisc-export95                       1:9.9.5.dfsg-9+deb8u9                amd64        Exported ISC Shared Library
ii  libisc95                              1:9.9.5.dfsg-9+deb8u9                amd64        ISC Shared Library used by BIND
ii  libisccc90                            1:9.9.5.dfsg-9+deb8u9                amd64        Command Channel Library used by BIND
ii  libisccfg-export90                    1:9.9.5.dfsg-9+deb8u9                amd64        Exported ISC CFG Shared Library
ii  libisccfg90                           1:9.9.5.dfsg-9+deb8u9                amd64        Config File Handling Library used by BIND
ii  libisl10:amd64                        0.12.2-2                             amd64        manipulating sets and relations of integer points bounded by linear constraints
ii  libiso9660-8                          0.83-4.2                             amd64        library to work with ISO9660 filesystems
ii  libisofs6                             1.3.2-1.1                            amd64        library to create ISO9660 images
ii  libisorelax-java                      20041111-8                           all          Interface for applications to support RELAX Core
ii  libitm1:amd64                         4.9.2-10                             amd64        GNU Transactional Memory Library
ii  libiw30:amd64                         30~pre9-8                            amd64        Wireless tools - library
ii  libjack-jackd2-0:amd64                1.9.10+20140719git3eb0ae6a~dfsg-2    amd64        JACK Audio Connection Kit (libraries)
ii  libjas-java                           2.5.4408-1                           all          Java object-oriented type-safe Algebra System
ii  libjas-plotter-java                   2.2.6+dfsg1-2                        all          JAS(2) Plotter graphic library
ii  libjasper1:amd64                      1.900.1-debian1-2.4+deb8u2           amd64        JasPer JPEG-2000 runtime library
ii  libjasper1:i386                       1.900.1-debian1-2.4+deb8u2           i386         JasPer JPEG-2000 runtime library
ii  libjaxen-java                         1.1.6-1                              all          Java XPath engine
ii  libjbig0:amd64                        2.1-3.1                              amd64        JBIGkit libraries
ii  libjbig0:i386                         2.1-3.1                              i386         JBIGkit libraries
ii  libjbig2dec0                          0.11+20120125-1                      amd64        JBIG2 decoder library - shared libraries
ii  libjcommon-java                       1.0.16-4                             all          General Purpose library for Java
ii  libjdom1-java                         1.1.3-1                              all          lightweight and fast library using XML
ii  libjfreechart-java                    1.0.13-7                             all          Chart library for Java
ii  libjfugue-java                        4.0.3-3                              all          Java API for music programming
ii  libjim0.75:amd64                      0.75-1                               amd64        small-footprint implementation of Tcl - shared library
ii  libjlatexmath-java                    1.0.3-1                              all          Implementation of LaTeX math mode wrote in Java
ii  libjpeg-turbo-progs                   1:1.3.1-12                           amd64        Programs for manipulating JPEG files
ii  libjpeg62-turbo:amd64                 1:1.3.1-12                           amd64        libjpeg-turbo JPEG runtime library
ii  libjpeg62-turbo:i386                  1:1.3.1-12                           i386         libjpeg-turbo JPEG runtime library
ii  libjs-jquery                          1.7.2+dfsg-3.2                       all          JavaScript library for dynamic web applications
ii  libjs-sphinxdoc                       1.2.3+dfsg-1                         all          JavaScript support for Sphinx documentation
ii  libjs-underscore                      1.7.0~dfsg-1                         all          JavaScript's functional programming helper library
ii  libjson-c2:amd64                      0.11-4                               amd64        JSON manipulation library - shared library
ii  libjson-glib-1.0-0:amd64              1.0.2-1                              amd64        GLib JSON manipulation library
ii  libjson-glib-1.0-common               1.0.2-1                              all          GLib JSON manipulation library (common files)
ii  libjte1                               1.20-1                               amd64        Jigdo Template Export - runtime library
ii  libk5crypto3:amd64                    1.12.1+dfsg-19+deb8u2                amd64        MIT Kerberos runtime libraries - Crypto Library
ii  libk5crypto3:i386                     1.12.1+dfsg-19+deb8u2                i386         MIT Kerberos runtime libraries - Crypto Library
ii  libkate1                              0.4.1-4                              amd64        Codec for karaoke and text encapsulation
ii  libkeybinder-3.0-0:amd64              0.3.0-1                              amd64        registers global key bindings for applications - Gtk+3
ii  libkeybinder0                         0.3.0-3                              amd64        registers global key bindings for applications
ii  libkeyutils1:amd64                    1.5.9-5+b1                           amd64        Linux Key Management Utilities (library)
ii  libkeyutils1:i386                     1.5.9-5+b1                           i386         Linux Key Management Utilities (library)
ii  libklibc                              2.0.4-2                              amd64        minimal libc subset for use with initramfs
ii  libkmod2:amd64                        18-3                                 amd64        libkmod shared library
ii  libkpathsea6                          2014.20140926.35254-6                amd64        TeX Live: path search library for TeX (runtime part)
ii  libkrb5-3:amd64                       1.12.1+dfsg-19+deb8u2                amd64        MIT Kerberos runtime libraries
ii  libkrb5-3:i386                        1.12.1+dfsg-19+deb8u2                i386         MIT Kerberos runtime libraries
ii  libkrb5support0:amd64                 1.12.1+dfsg-19+deb8u2                amd64        MIT Kerberos runtime libraries - Support library
ii  libkrb5support0:i386                  1.12.1+dfsg-19+deb8u2                i386         MIT Kerberos runtime libraries - Support library
ii  liblangtag-common                     0.5.1-3                              all          library to access tags for identifying languages -- data
ii  liblangtag1                           0.5.1-3                              amd64        library to access tags for identifying languages
ii  liblapack3                            3.5.0-4                              amd64        Library of linear algebra routines 3 - shared version
ii  liblcms2-2:amd64                      2.6-3+deb8u1                         amd64        Little CMS 2 color management library
ii  libldap-2.4-2:amd64                   2.4.40+dfsg-1+deb8u2                 amd64        OpenLDAP libraries
ii  libldb1:amd64                         2:1.1.20-0+deb8u1                    amd64        LDAP-like embedded database - shared library
ii  liblightdm-gobject-1-0                1.10.3-3                             amd64        simple display manager (gobject library)
ii  liblircclient0                        0.9.0~pre1-1.2                       amd64        infra-red remote control support - client library
ii  liblivemedia23                        2014.01.13-1                         amd64        multimedia RTSP streaming library
ii  libllvm3.5:amd64                      1:3.5-10                             amd64        Modular compiler and toolchain technologies, runtime library
ii  liblocale-gettext-perl                1.05-8+b1                            amd64        module using libc functions for internationalization in Perl
ii  liblockfile-bin                       1.09-6                               amd64        support binaries for and cli utilities based on liblockfile
ii  liblockfile1:amd64                    1.09-6                               amd64        NFS-safe locking library
ii  liblog-message-perl                   0.8-1                                all          powerful and flexible message logging mechanism
ii  liblog-message-simple-perl            0.10-2                               all          simplified interface to Log::Message
ii  liblog4j1.2-java                      1.2.17-5                             all          Logging library for java
ii  liblogging-stdlog0:amd64              1.0.4-1                              amd64        easy to use and lightweight logging library
ii  liblognorm1:amd64                     1.0.1-3                              amd64        Log normalizing library
ii  liblouis-data                         2.5.3-3                              all          Braille translation library - data
ii  liblouis2:amd64                       2.5.3-3                              amd64        Braille translation library - shared libs
ii  liblqr-1-0:amd64                      0.4.2-2                              amd64        converts plain array images into multi-size representation
ii  liblsan0:amd64                        4.9.2-10                             amd64        LeakSanitizer -- a memory leak detector (runtime)
ii  libltdl7:amd64                        2.4.2-1.11+b1                        amd64        System independent dlopen wrapper for GNU libtool
ii  liblua5.2-0:amd64                     5.2.3-1.1                            amd64        Shared library for the Lua interpreter version 5.2
ii  liblwp-mediatypes-perl                6.02-1                               all          module to guess media type for a file or a URL
ii  liblwp-protocol-https-perl            6.06-2                               all          HTTPS driver for LWP::UserAgent
ii  liblwres90                            1:9.9.5.dfsg-9+deb8u9                amd64        Lightweight Resolver Library used by BIND
ii  liblzma5:amd64                        5.1.1alpha+20120614-2+b3             amd64        XZ-format compression library
ii  liblzma5:i386                         5.1.1alpha+20120614-2+b3             i386         XZ-format compression library
ii  liblzo2-2:amd64                       2.08-1.2                             amd64        data compression library
ii  libm17n-0                             1.6.4-3                              amd64        multilingual text processing library - runtime
ii  libmad0:amd64                         0.15.1b-8                            amd64        MPEG audio decoder library
ii  libmagic1:amd64                       1:5.22+15-2+deb8u3                   amd64        File type determination library using "magic" numbers
ii  libmagickcore-6.q16-2:amd64           8:6.8.9.9-5+deb8u6                   amd64        low-level image manipulation library -- quantum depth Q16
ii  libmagickwand-6.q16-2:amd64           8:6.8.9.9-5+deb8u6                   amd64        image manipulation library
ii  libmailtools-perl                     2.13-1                               all          Manipulate email in perl programs
ii  libmatroska6:amd64                    1.4.1-2+deb8u1                       amd64        extensible open standard audio/video container format (shared library)
ii  libmbim-glib4:amd64                   1.10.0-2.1                           amd64        Support library to use the MBIM protocol
ii  libmbim-proxy                         1.10.0-2.1                           amd64        Proxy to communicate with MBIM ports
ii  libmhash2:amd64                       0.9.9.9-7                            amd64        Library for cryptographic hashing and message authentication
ii  libmm-glib0:amd64                     1.4.0-1                              amd64        D-Bus service for managing modems - shared libraries
ii  libmng1:amd64                         1.0.10+dfsg-3.1+b3                   amd64        Multiple-image Network Graphics library
ii  libmnl0:amd64                         1.0.3-5                              amd64        minimalistic Netlink communication library
ii  libmodplug1                           1:0.8.8.4-4.1+b1                     amd64        shared libraries for mod music based on ModPlug
ii  libmodule-build-perl                  0.421000-2+deb8u1                    all          framework for building and installing Perl modules
ii  libmodule-pluggable-perl              5.1-1                                all          module for giving  modules the ability to have plugins
ii  libmodule-signature-perl              0.73-1+deb8u2                        all          module to manipulate CPAN SIGNATURE files
ii  libmount1:amd64                       2.25.2-6                             amd64        device mounting library
ii  libmp3lame0:amd64                     3.99.5+repack1-7+deb8u1              amd64        MP3 encoding library
ii  libmpc3:amd64                         1.0.2-1                              amd64        multiple precision complex floating-point library
ii  libmpcdec6:amd64                      2:0.1~r459-4.1                       amd64        MusePack decoder - library
ii  libmpdec2:amd64                       2.4.1-1                              amd64        library for decimal floating point arithmetic (runtime library)
ii  libmpeg2-4:amd64                      0.5.1-7                              amd64        MPEG1 and MPEG2 video decoder library
ii  libmpfr4:amd64                        3.1.2-2                              amd64        multiple precision floating-point computation
ii  libmro-compat-perl                    0.12-1                               all          mro::* interface compatibility for Perls < 5.9.5
ii  libmspub-0.1-1                        0.1.1-2                              amd64        library for parsing the mspub file structure
ii  libmsv-java                           2009.1+dfsg1-4                       all          Sun multi-schema XML validator
ii  libmtdev1:amd64                       1.1.5-1                              amd64        Multitouch Protocol Translation Library - shared library
ii  libmtp-common                         1.1.8-1                              all          Media Transfer Protocol (MTP) common files
ii  libmtp-runtime                        1.1.8-1+b1                           amd64        Media Transfer Protocol (MTP) runtime tools
ii  libmtp9:amd64                         1.1.8-1+b1                           amd64        Media Transfer Protocol (MTP) library
ii  libmwaw-0.3-3                         0.3.1-2                              amd64        import library for some old Mac text documents
ii  libmythes-1.2-0                       2:1.2.4-1                            amd64        simple thesaurus library
ii  libnb-org-openide-util-java           7.4+dfsg1-2                          all          Utility classes from the NetBeans Platform
ii  libnb-org-openide-util-lookup-java    7.4+dfsg1-2                          all          Utility lookup classes from the NetBeans Platform
ii  libncurses5:amd64                     5.9+20140913-1+b1                    amd64        shared libraries for terminal handling
ii  libncurses5-dev:amd64                 5.9+20140913-1+b1                    amd64        developer's libraries for ncurses
ii  libncursesw5:amd64                    5.9+20140913-1+b1                    amd64        shared libraries for terminal handling (wide character support)
ii  libndp0:amd64                         1.4-2+deb8u1                         amd64        Library for Neighbor Discovery Protocol
ii  libneon27-gnutls                      0.30.1-1                             amd64        HTTP and WebDAV client library (GnuTLS enabled)
ii  libnet-dbus-perl                      1.0.0-2+b2                           amd64        Perl extension for the DBus bindings
ii  libnet-http-perl                      6.07-1                               all          module providing low-level HTTP connection client
ii  libnet-smtp-ssl-perl                  1.01-3                               all          Perl module providing SSL support to Net::SMTP
ii  libnet-ssleay-perl                    1.65-1+deb8u1                        amd64        Perl module for Secure Sockets Layer (SSL)
ii  libnetfilter-acct1:amd64              1.0.2-1.1                            amd64        Netfilter acct library
ii  libnetfilter-conntrack3:amd64         1.0.4-1                              amd64        Netfilter netlink-conntrack library
ii  libnettle4:amd64                      2.7.1-5+deb8u2                       amd64        low level cryptographic library (symmetric and one-way cryptos)
ii  libnettle4:i386                       2.7.1-5+deb8u2                       i386         low level cryptographic library (symmetric and one-way cryptos)
ii  libnewt0.52:amd64                     0.52.17-1+b1                         amd64        Not Erik's Windowing Toolkit - text mode windowing with slang
ii  libnfnetlink0:amd64                   1.0.1-3                              amd64        Netfilter netlink library
ii  libnl-3-200:amd64                     3.2.24-2                             amd64        library for dealing with netlink sockets
ii  libnl-genl-3-200:amd64                3.2.24-2                             amd64        library for dealing with netlink sockets - generic netlink
ii  libnl-route-3-200:amd64               3.2.24-2                             amd64        library for dealing with netlink sockets - route interface
ii  libnm-glib-vpn1:amd64                 0.9.10.0-7                           amd64        network management framework (GLib VPN shared library)
ii  libnm-glib4:amd64                     0.9.10.0-7                           amd64        network management framework (GLib shared library)
ii  libnm-gtk-common                      0.9.10.0-2                           all          library for wireless and mobile dialogs - common files
ii  libnm-gtk0:amd64                      0.9.10.0-2                           amd64        library for wireless and mobile dialogs
ii  libnm-util2:amd64                     0.9.10.0-7                           amd64        network management framework (shared library)
ii  libnotify-bin                         0.7.6-2                              amd64        sends desktop notifications to a notification daemon (Utilities)
ii  libnotify4:amd64                      0.7.6-2                              amd64        sends desktop notifications to a notification daemon
ii  libnspr4:amd64                        2:4.12-1+debu8u1                     amd64        NetScape Portable Runtime Library
ii  libnspr4:i386                         2:4.12-1+debu8u1                     i386         NetScape Portable Runtime Library
ii  libnss-mdns:amd64                     0.10-6                               amd64        NSS module for Multicast DNS name resolution
ii  libnss3:amd64                         2:3.26-1+debu8u1                     amd64        Network Security Service libraries
ii  libnss3:i386                          2:3.26-1+debu8u1                     i386         Network Security Service libraries
ii  libntdb1:amd64                        1.0-5                                amd64        New Trivial Database - shared library
ii  libnuma1:amd64                        2.0.10-1                             amd64        Libraries for controlling NUMA policy
ii  libodfgen-0.1-1                       0.1.1-2                              amd64        library to generate ODF documents
ii  libogg0:amd64                         1.3.2-1                              amd64        Ogg bitstream library
ii  libopencore-amrnb0:amd64              0.1.3-2.1                            amd64        Adaptive Multi Rate speech codec - shared library
ii  libopencore-amrwb0:amd64              0.1.3-2.1                            amd64        Adaptive Multi-Rate - Wideband speech codec - shared library
ii  libopenexr6:amd64                     1.6.1-8                              amd64        runtime files for the OpenEXR image library
ii  libopenjpeg5:amd64                    1:1.5.2-3                            amd64        JPEG 2000 image compression/decompression library - runtime
ii  libopenraw1:amd64                     0.0.9-3.5+b2                         amd64        free implementation for RAW decoding
ii  libopenvg1-mesa:amd64                 10.3.2-1+deb8u1                      amd64        free implementation of the OpenVG API -- runtime
ii  libopus0:amd64                        1.1-2                                amd64        Opus codec runtime library
ii  liborbit-2-0:amd64                    1:2.14.19-0.3                        amd64        high-performance CORBA implementation - common libraries
ii  liborc-0.4-0:amd64                    1:0.4.22-1                           amd64        Library of Optimized Inner Loops Runtime Compiler
ii  liborcus-0.8-0                        0.7.0+dfsg-9                         amd64        library for processing spreadsheet documents
ii  libotf0                               0.9.13-2                             amd64        Library for handling OpenType Font - runtime
ii  libp11-kit0:amd64                     0.20.7-1                             amd64        Library for loading and coordinating access to PKCS#11 modules - runtime
ii  libp11-kit0:i386                      0.20.7-1                             i386         Library for loading and coordinating access to PKCS#11 modules - runtime
ii  libpackage-constants-perl             0.04-1                               all          List constants defined in a package
ii  libpackagekit-glib2-18:amd64          1.0.1-2                              amd64        Library for accessing PackageKit using GLib
ii  libpam-gnome-keyring                  3.14.0-1+b1                          amd64        PAM module to unlock the GNOME keyring upon login
ii  libpam-modules:amd64                  1.1.8-3.1+deb8u2                     amd64        Pluggable Authentication Modules for PAM
ii  libpam-modules-bin                    1.1.8-3.1+deb8u2                     amd64        Pluggable Authentication Modules for PAM - helper binaries
ii  libpam-runtime                        1.1.8-3.1+deb8u2                     all          Runtime support for the PAM library
ii  libpam-systemd:amd64                  215-17+deb8u6                        amd64        system and service manager - PAM module
ii  libpam0g:amd64                        1.1.8-3.1+deb8u2                     amd64        Pluggable Authentication Modules library
ii  libpango-1.0-0:amd64                  1.36.8-3                             amd64        Layout and rendering of internationalized text
ii  libpango-1.0-0:i386                   1.36.8-3                             i386         Layout and rendering of internationalized text
ii  libpango-perl                         1.226-2                              amd64        Perl module to layout and render international text
ii  libpango1.0-0:amd64                   1.36.8-3                             amd64        Layout and rendering of internationalized text
ii  libpangocairo-1.0-0:amd64             1.36.8-3                             amd64        Layout and rendering of internationalized text
ii  libpangocairo-1.0-0:i386              1.36.8-3                             i386         Layout and rendering of internationalized text
ii  libpangoft2-1.0-0:amd64               1.36.8-3                             amd64        Layout and rendering of internationalized text
ii  libpangoft2-1.0-0:i386                1.36.8-3                             i386         Layout and rendering of internationalized text
ii  libpangox-1.0-0:amd64                 0.0.2-5                              amd64        pango library X backend
ii  libpangoxft-1.0-0:amd64               1.36.8-3                             amd64        Layout and rendering of internationalized text
ii  libpaper-utils                        1.1.24+nmu4                          amd64        library for handling paper characteristics (utilities)
ii  libpaper1:amd64                       1.1.24+nmu4                          amd64        library for handling paper characteristics
ii  libparams-util-perl                   1.07-2+b1                            amd64        Perl extension for simple stand-alone param checking functions
ii  libparted2:amd64                      3.2-7                                amd64        disk partition manipulator - shared library
ii  libpcap0.8:amd64                      1.6.2-2                              amd64        system interface for user-level packet capture
ii  libpciaccess0:amd64                   0.13.2-3+b1                          amd64        Generic PCI access library for X
ii  libpcre3:amd64                        2:8.35-3.3+deb8u4                    amd64        Perl 5 Compatible Regular Expression Library - runtime files
ii  libpcre3:i386                         2:8.35-3.3+deb8u4                    i386         Perl 5 Compatible Regular Expression Library - runtime files
ii  libpcsclite1:amd64                    1.8.13-1+deb8u1                      amd64        Middleware to access a smart card using PC/SC (library)
ii  libpipeline1:amd64                    1.4.0-1                              amd64        pipeline manipulation library
ii  libpixman-1-0:amd64                   0.32.6-3                             amd64        pixel-manipulation library for X and cairo
ii  libpixman-1-0:i386                    0.32.6-3                             i386         pixel-manipulation library for X and cairo
ii  libplist2:amd64                       1.11-3                               amd64        Library for handling Apple binary and XML property lists
ii  libpng12-0:amd64                      1.2.50-2+deb8u3                      amd64        PNG library - runtime
ii  libpng12-0:i386                       1.2.50-2+deb8u3                      i386         PNG library - runtime
ii  libpod-latex-perl                     0.61-1                               all          module to convert Pod data to formatted LaTeX
ii  libpod-readme-perl                    0.11-1                               all          Perl module to convert POD to README file
ii  libpolkit-agent-1-0:amd64             0.105-15~deb8u2                      amd64        PolicyKit Authentication Agent API
ii  libpolkit-backend-1-0:amd64           0.105-15~deb8u2                      amd64        PolicyKit backend API
ii  libpolkit-gobject-1-0:amd64           0.105-15~deb8u2                      amd64        PolicyKit Authorization API
ii  libpoppler-glib8:amd64                0.26.5-2+deb8u1                      amd64        PDF rendering library (GLib-based shared library)
ii  libpoppler46:amd64                    0.26.5-2+deb8u1                      amd64        PDF rendering library
ii  libpopt0:amd64                        1.16-10                              amd64        lib for parsing cmdline parameters
ii  libportaudio2:amd64                   19+svn20140130-1                     amd64        Portable audio I/O - shared library
ii  libpostproc52                         6:0.git20120821-4                    amd64        FFmpeg derived postprocessing library
ii  libprocps3:amd64                      2:3.3.9-9                            amd64        library for accessing process information from /proc
ii  libproxy-tools                        0.4.11-4+b2                          amd64        automatic proxy configuration management library (tools)
ii  libproxy1:amd64                       0.4.11-4+b2                          amd64        automatic proxy configuration management library (shared)
ii  libpsl0:amd64                         0.5.1-1                              amd64        Library for Public Suffix List (shared libraries)
ii  libpthread-stubs0-dev:amd64           0.3-4                                amd64        pthread stubs not provided by native libc, development files
ii  libpulse0:amd64                       5.0-13                               amd64        PulseAudio client libraries
ii  libpulsedsp:amd64                     5.0-13                               amd64        PulseAudio OSS pre-load library
ii  libpython-stdlib:amd64                2.7.9-1                              amd64        interactive high-level object-oriented language (default python version)
ii  libpython2.7:amd64                    2.7.9-2+deb8u1                       amd64        Shared Python runtime library (version 2.7)
ii  libpython2.7-minimal:amd64            2.7.9-2+deb8u1                       amd64        Minimal subset of the Python language (version 2.7)
ii  libpython2.7-stdlib:amd64             2.7.9-2+deb8u1                       amd64        Interactive high-level object-oriented language (standard library, version 2.7)
ii  libpython3-stdlib:amd64               3.4.2-2                              amd64        interactive high-level object-oriented language (default python3 version)
ii  libpython3.4:amd64                    3.4.2-1                              amd64        Shared Python runtime library (version 3.4)
ii  libpython3.4-minimal:amd64            3.4.2-1                              amd64        Minimal subset of the Python language (version 3.4)
ii  libpython3.4-stdlib:amd64             3.4.2-1                              amd64        Interactive high-level object-oriented language (standard library, version 3.4)
ii  libqmi-glib1:amd64                    1.10.2-2                             amd64        Support library to use the Qualcomm MSM Interface (QMI) protocol
ii  libqmi-proxy                          1.10.2-2                             amd64        Proxy to communicate with QMI ports
ii  libqt4-dbus:amd64                     4:4.8.6+git64-g5dc8b2b+dfsg-3+deb8u1 amd64        Qt 4 D-Bus module
ii  libqt4-xml:amd64                      4:4.8.6+git64-g5dc8b2b+dfsg-3+deb8u1 amd64        Qt 4 XML module
ii  libqtcore4:amd64                      4:4.8.6+git64-g5dc8b2b+dfsg-3+deb8u1 amd64        Qt 4 core module
ii  libqtdbus4:amd64                      4:4.8.6+git64-g5dc8b2b+dfsg-3+deb8u1 amd64        Qt 4 D-Bus module library
ii  libqtgui4:amd64                       4:4.8.6+git64-g5dc8b2b+dfsg-3+deb8u1 amd64        Qt 4 GUI module
ii  libquadmath0:amd64                    4.9.2-10                             amd64        GCC Quad-Precision Math Library
ii  libraptor2-0:amd64                    2.0.14-1                             amd64        Raptor 2 RDF syntax library
ii  librarian0                            0.8.1-6                              amd64        Documentation meta-data library (library package)
ii  librasqal3:amd64                      0.9.32-1                             amd64        Rasqal RDF query library
ii  libraw1394-11:amd64                   2.1.0-3                              amd64        library for direct access to IEEE 1394 bus (aka FireWire)
ii  librdf0:amd64                         1.0.17-1+b1                          amd64        Redland Resource Description Framework (RDF) library
ii  libreadline6:amd64                    6.3-8+b3                             amd64        GNU readline and history libraries, run-time libraries
ii  libregexp-common-perl                 2013031301-1                         all          module with common regular expressions
ii  librelaxng-datatype-java              1.0+ds1-3                            all          Java datatype interface for RELAX NG
ii  libreoffice                           1:4.3.3-2+deb8u5                     amd64        office productivity suite (metapackage)
ii  libreoffice-avmedia-backend-gstreamer 1:4.3.3-2+deb8u5                     amd64        GStreamer backend for LibreOffice
ii  libreoffice-base                      1:4.3.3-2+deb8u5                     amd64        office productivity suite -- database
ii  libreoffice-base-core                 1:4.3.3-2+deb8u5                     amd64        office productivity suite -- shared library
ii  libreoffice-base-drivers              1:4.3.3-2+deb8u5                     amd64        Database connectivity drivers for LibreOffice
ii  libreoffice-calc                      1:4.3.3-2+deb8u5                     amd64        office productivity suite -- spreadsheet
ii  libreoffice-common                    1:4.3.3-2+deb8u5                     all          office productivity suite -- arch-independent files
ii  libreoffice-core                      1:4.3.3-2+deb8u5                     amd64        office productivity suite -- arch-dependent files
ii  libreoffice-draw                      1:4.3.3-2+deb8u5                     amd64        office productivity suite -- drawing
ii  libreoffice-gtk                       1:4.3.3-2+deb8u5                     amd64        office productivity suite -- GTK+ integration
ii  libreoffice-help-en-us                1:4.3.3-2+deb8u5                     all          office productivity suite -- English_american help
ii  libreoffice-impress                   1:4.3.3-2+deb8u5                     amd64        office productivity suite -- presentation
ii  libreoffice-java-common               1:4.3.3-2+deb8u5                     all          office productivity suite -- arch-independent Java support files
ii  libreoffice-math                      1:4.3.3-2+deb8u5                     amd64        office productivity suite -- equation editor
ii  libreoffice-report-builder-bin        1:4.3.3-2+deb8u5                     amd64        LibreOffice component for building database reports -- libraries
ii  libreoffice-sdbc-firebird             1:4.3.3-2+deb8u5                     amd64        Firebird SDBC driver for LibreOffice
ii  libreoffice-sdbc-hsqldb               1:4.3.3-2+deb8u5                     amd64        HSQLDB SDBC driver for LibreOffice
ii  libreoffice-style-galaxy              1:4.3.3-2+deb8u5                     all          office productivity suite -- Galaxy (Default) symbol style
ii  libreoffice-style-tango               1:4.3.3-2+deb8u5                     all          office productivity suite -- Tango symbol style
ii  libreoffice-writer                    1:4.3.3-2+deb8u5                     amd64        office productivity suite -- word processor
ii  libresid-builder0c2a                  2.1.1-14                             amd64        SID chip emulation class based on resid
ii  librest-0.7-0:amd64                   0.7.92-3                             amd64        REST service access library
ii  librevenge-0.0-0                      0.0.1-3                              amd64        Base Library for writing document interface filters
ii  librhino-java                         1.7R4-3                              all          Libraries for rhino Java Script Engine
ii  librsvg2-2:amd64                      2.40.5-1+deb8u2                      amd64        SAX-based renderer library for SVG files (runtime)
ii  librsvg2-common:amd64                 2.40.5-1+deb8u2                      amd64        SAX-based renderer library for SVG files (extra runtime)
ii  librtmp1:amd64                        2.4+20150115.gita107cef-1            amd64        toolkit for RTMP streams (shared library)
ii  libsamplerate0:amd64                  0.1.8-8                              amd64        Audio sample rate conversion library
ii  libsane:amd64                         1.0.24-8+deb8u1                      amd64        API library for scanners
ii  libsane-common                        1.0.24-8+deb8u1                      all          API library for scanners -- documentation and support files
ii  libsane-extras:amd64                  1.0.22.3                             amd64        API library for scanners -- extra backends
ii  libsane-extras-common                 1.0.22.3                             amd64        API library for scanners -- documentation and support files
ii  libsasl2-2:amd64                      2.1.26.dfsg1-13+deb8u1               amd64        Cyrus SASL - authentication abstraction library
ii  libsasl2-modules:amd64                2.1.26.dfsg1-13+deb8u1               amd64        Cyrus SASL - pluggable authentication modules
ii  libsasl2-modules-db:amd64             2.1.26.dfsg1-13+deb8u1               amd64        Cyrus SASL - pluggable authentication modules (DB)
ii  libschroedinger-1.0-0:amd64           1.0.11-2.1                           amd64        library for encoding/decoding of Dirac video streams
ii  libsctp1:amd64                        1.0.16+dfsg-2                        amd64        user-space access to Linux Kernel SCTP - shared library
ii  libsdl-image1.2:amd64                 1.2.12-5+b5                          amd64        Image loading library for Simple DirectMedia Layer 1.2, libraries
ii  libsdl1.2debian:amd64                 1.2.15-10+b1                         amd64        Simple DirectMedia Layer
ii  libsecret-1-0:amd64                   0.18-1+b1                            amd64        Secret store
ii  libsecret-common                      0.18-1                               all          Secret store (common files)
ii  libselinux1:amd64                     2.3-2                                amd64        SELinux runtime shared libraries
ii  libselinux1:i386                      2.3-2                                i386         SELinux runtime shared libraries
ii  libsemanage-common                    2.3-1                                all          Common files for SELinux policy management libraries
ii  libsemanage1:amd64                    2.3-1+b1                             amd64        SELinux policy management library
ii  libsensors4:amd64                     1:3.3.5-2                            amd64        library to read temperature/voltage/fan sensors
ii  libsepol1:amd64                       2.3-2                                amd64        SELinux library for manipulating binary security policies
ii  libservlet2.5-java                    6.0.45+dfsg-1~deb8u1                 all          Servlet 2.5 and JSP 2.1 Java API classes
ii  libsgutils2-2                         1.39-1                               amd64        utilities for devices using the SCSI command set (shared libraries)
ii  libshine3:amd64                       3.1.0-2.1                            amd64        Fixed-point MP3 encoding library - runtime files
ii  libshout3:amd64                       2.3.1-3                              amd64        MP3/Ogg Vorbis broadcast streaming library
ii  libsidplay1                           1.36.59-6                            amd64        SID (MOS 6581) emulation library
ii  libsidplay2                           2.1.1-14                             amd64        SID (MOS 6581) emulation library
ii  libsigc++-2.0-0c2a:amd64              2.4.0-1                              amd64        type-safe Signal Framework for C++ - runtime
ii  libslang2:amd64                       2.3.0-2                              amd64        S-Lang programming library - runtime version
ii  libsm-dev:amd64                       2:1.2.2-1+b1                         amd64        X11 Session Management library (development headers)
ii  libsm6:amd64                          2:1.2.2-1+b1                         amd64        X11 Session Management library
ii  libsm6:i386                           2:1.2.2-1+b1                         i386         X11 Session Management library
ii  libsmartcols1:amd64                   2.25.2-6                             amd64        smart column output alignment library
ii  libsmbclient:amd64                    2:4.2.14+dfsg-0+deb8u2               amd64        shared library for communication with SMB/CIFS servers
ii  libsndfile1:amd64                     1.0.25-9.1+deb8u1                    amd64        Library for reading/writing audio files
ii  libsoftware-license-perl              0.103010-3                           all          module providing templated software licenses
ii  libsonic0:amd64                       0.1.17-1.1                           amd64        Simple library to speed up or slow down speech
ii  libsoup-gnome2.4-1:amd64              2.48.0-1                             amd64        HTTP library implementation in C -- GNOME support library
ii  libsoup2.4-1:amd64                    2.48.0-1                             amd64        HTTP library implementation in C -- Shared library
ii  libspectre1:amd64                     0.2.7-3                              amd64        Library for rendering PostScript documents
ii  libspeechd2:amd64                     0.8-7                                amd64        Speech Dispatcher: Shared libraries
ii  libspeex1:amd64                       1.2~rc1.2-1                          amd64        The Speex codec runtime library
ii  libspeexdsp1:amd64                    1.2~rc1.2-1                          amd64        The Speex extended runtime library
ii  libsqlite3-0:amd64                    3.8.7.1-1+deb8u2                     amd64        SQLite 3 shared library
ii  libsqlite3-0:i386                     3.8.7.1-1+deb8u2                     i386         SQLite 3 shared library
ii  libss2:amd64                          1.42.12-2+b1                         amd64        command-line interface parsing library
ii  libssh2-1:amd64                       1.4.3-4.1+deb8u1                     amd64        SSH2 client-side library
ii  libssl1.0.0:amd64                     1.0.1t-1+deb8u6                      amd64        Secure Sockets Layer toolkit - shared libraries
ii  libstartup-notification0:amd64        0.12-4                               amd64        library for program launch feedback (shared library)
ii  libstdc++-4.9-dev:amd64               4.9.2-10                             amd64        GNU Standard C++ Library v3 (development files)
ii  libstdc++6:amd64                      4.9.2-10                             amd64        GNU Standard C++ Library v3
ii  libstdc++6:i386                       4.9.2-10                             i386         GNU Standard C++ Library v3
ii  libsub-exporter-perl                  0.986-1                              all          sophisticated exporter for custom-built routines
ii  libsub-install-perl                   0.928-1                              all          module for installing subroutines into packages easily
ii  libswscale3:amd64                     6:11.8-1~deb8u1                      amd64        Libav video scaling library
ii  libsystemd0:amd64                     215-17+deb8u6                        amd64        systemd utility library
ii  libtablelayout-java                   20090826-2                           all          Java layout manager for creating user interfaces fast and easy
ii  libtag1-vanilla:amd64                 1.9.1-2.1                            amd64        audio meta-data library - vanilla flavour
ii  libtag1c2a:amd64                      1.9.1-2.1                            amd64        audio meta-data library
ii  libtagc0:amd64                        1.9.1-2.1                            amd64        audio meta-data library - C bindings
ii  libtalloc2:amd64                      2.1.2-0+deb8u1                       amd64        hierarchical pool based memory allocator
ii  libtasn1-6:amd64                      4.2-3+deb8u2                         amd64        Manage ASN.1 structures (runtime)
ii  libtasn1-6:i386                       4.2-3+deb8u2                         i386         Manage ASN.1 structures (runtime)
ii  libtbb2                               4.2~20140122-5                       amd64        parallelism library for C++ - runtime files
ii  libtcl8.6:amd64                       8.6.2+dfsg-2                         amd64        Tcl (the Tool Command Language) v8.6 - run-time library files
ii  libtdb1:amd64                         1.3.6-0+deb8u1                       amd64        Trivial Database - shared library
ii  libteamdctl0:amd64                    1.12-2                               amd64        library for communication with `teamd` process
ii  libterm-ui-perl                       0.42-1                               all          Term::ReadLine UI made easy
ii  libtevent0:amd64                      0.9.28-0+deb8u1                      amd64        talloc-based event loop library - shared library
ii  libtext-charwidth-perl                0.04-7+b3                            amd64        get display widths of characters on the terminal
ii  libtext-iconv-perl                    1.7-5+b2                             amd64        converts between character sets in Perl
ii  libtext-soundex-perl                  3.4-1+b2                             amd64        implementation of the soundex algorithm
ii  libtext-template-perl                 1.46-1                               all          perl module to process text templates
ii  libtext-wrapi18n-perl                 0.06-7                               all          internationalized substitute of Text::Wrap
ii  libthai-data                          0.1.21-1                             all          Data files for Thai language support library
ii  libthai0:amd64                        0.1.21-1                             amd64        Thai language support library
ii  libthai0:i386                         0.1.21-1                             i386         Thai language support library
ii  libtheora0:amd64                      1.1.1+dfsg.1-6                       amd64        Theora Video Compression Codec
ii  libthunarx-2-0                        1.6.3-2                              amd64        extension library for thunar
ii  libtidy-0.99-0                        20091223cvs-1.4+deb8u1               amd64        HTML syntax checker and reformatter - library
ii  libtie-ixhash-perl                    1.23-1                               all          Perl module to order associative arrays
ii  libtiff5:amd64                        4.0.3-12.3+deb8u2                    amd64        Tag Image File Format (TIFF) library
ii  libtiff5:i386                         4.0.3-12.3+deb8u2                    i386         Tag Image File Format (TIFF) library
ii  libtimedate-perl                      2.3000-2                             all          collection of modules to manipulate date/time information
ii  libtinfo-dev:amd64                    5.9+20140913-1+b1                    amd64        developer's library for the low-level terminfo library
ii  libtinfo5:amd64                       5.9+20140913-1+b1                    amd64        shared low-level terminfo library for terminal handling
ii  libtk8.6:amd64                        8.6.2-1                              amd64        Tk toolkit for Tcl and X11 v8.6 - run-time files
ii  libtsan0:amd64                        4.9.2-10                             amd64        ThreadSanitizer -- a Valgrind-based detector of data races (runtime)
ii  libtumbler-1-0                        0.1.30-1+b1                          amd64        library for tumbler, a D-Bus thumbnailing service
ii  libturbojpeg1:amd64                   1:1.3.1-12                           amd64        TurboJPEG runtime library - SIMD optimized
ii  libtwolame0                           0.3.13-1.1                           amd64        MPEG Audio Layer 2 encoding library
ii  libtxc-dxtn-s2tc0:amd64               0~git20131104-1.1                    amd64        Texture compression library for Mesa
ii  libubsan0:amd64                       4.9.2-10                             amd64        UBSan -- undefined behaviour sanitizer (runtime)
ii  libudev1:amd64                        215-17+deb8u6                        amd64        libudev shared library
ii  libudisks2-0:amd64                    2.1.3-5                              amd64        GObject based library to access udisks2
ii  libumfpack5.6.2:amd64                 1:4.2.1-3                            amd64        sparse LU factorization library
ii  libunique-1.0-0                       1.1.6-5                              amd64        Library for writing single instance applications - shared libraries
ii  libupnp6                              1:1.6.19+git20141001-1+deb8u1        amd64        Portable SDK for UPnP Devices, version 1.6 (shared libraries)
ii  libupower-glib3:amd64                 0.99.1-3.2                           amd64        abstraction for power management - shared library
ii  liburi-perl                           1.64-1                               all          module to manipulate and access URI strings
ii  libusageenvironment1                  2014.01.13-1                         amd64        multimedia RTSP streaming library (UsageEnvironment classes)
ii  libusb-0.1-4:amd64                    2:0.1.12-25                          amd64        userspace USB programming library
ii  libusb-1.0-0:amd64                    2:1.0.19-1                           amd64        userspace USB programming library
ii  libusbmuxd2:amd64                     1.0.9-1                              amd64        USB multiplexor daemon for iPhone and iPod Touch devices - library
ii  libustr-1.0-1:amd64                   1.0.4-3+b2                           amd64        Micro string library: shared library
ii  libutempter0                          1.1.5-4                              amd64        A privileged helper for utmp/wtmp updates (runtime)
ii  libuuid-perl                          0.05-1+b1                            amd64        Perl extension for using UUID interfaces as defined in e2fsprogs
ii  libuuid1:amd64                        2.25.2-6                             amd64        Universally Unique ID library
ii  libuuid1:i386                         2.25.2-6                             i386         Universally Unique ID library
ii  libv4l-0:amd64                        1.6.0-2                              amd64        Collection of video4linux support libraries
ii  libv4lconvert0:amd64                  1.6.0-2                              amd64        Video4linux frame format conversion library
ii  libva-drm1:amd64                      1.4.1-1                              amd64        Video Acceleration (VA) API for Linux -- DRM runtime
ii  libva-x11-1:amd64                     1.4.1-1                              amd64        Video Acceleration (VA) API for Linux -- X11 runtime
ii  libva1:amd64                          1.4.1-1                              amd64        Video Acceleration (VA) API for Linux -- runtime
ii  libvcdinfo0                           0.7.24+dfsg-0.2                      amd64        library to extract information from VideoCD
ii  libvdpau1:amd64                       0.8-3+deb8u2                         amd64        Video Decode and Presentation API for Unix (libraries)
ii  libvisio-0.1-1                        0.1.0-2                              amd64        library for parsing the visio file structure
ii  libvisual-0.4-0:amd64                 0.4.0-6                              amd64        Audio visualization framework
ii  libvisual-0.4-plugins:amd64           0.4.0.dfsg.1-7                       amd64        Audio visualization framework plugins
ii  libvlc5                               2.2.4-1~deb8u1                       amd64        multimedia player and streamer library
ii  libvlccore8                           2.2.4-1~deb8u1                       amd64        base library for VLC and its modules
ii  libvncclient0:amd64                   0.9.9+dfsg2-6.1+deb8u2               amd64        API to write one's own vnc server - client library
ii  libvorbis0a:amd64                     1.3.4-2                              amd64        decoder library for Vorbis General Audio Compression Codec
ii  libvorbisenc2:amd64                   1.3.4-2                              amd64        encoder library for Vorbis General Audio Compression Codec
ii  libvorbisfile3:amd64                  1.3.4-2                              amd64        high-level API for Vorbis General Audio Compression Codec
ii  libvpx1:amd64                         1.3.0-3                              amd64        VP8 and VP9 video codec (shared library)
ii  libvte-2.90-9                         1:0.36.3-1                           amd64        Terminal emulator widget for GTK+ 3.0 - runtime files
ii  libvte-2.90-common                    1:0.36.3-1                           all          Terminal emulator widget for GTK+ 3.0 - common files
ii  libvte-common                         1:0.28.2-5                           all          Terminal emulator widget for GTK+ 2.x - common files
ii  libvte9                               1:0.28.2-5                           amd64        Terminal emulator widget for GTK+ 2.0 - runtime files
ii  libwavpack1:amd64                     4.70.0-1                             amd64        audio codec (lossy and lossless) - library
ii  libwayland-client0:amd64              1.6.0-2                              amd64        wayland compositor infrastructure - client library
ii  libwayland-cursor0:amd64              1.6.0-2                              amd64        wayland compositor infrastructure - cursor library
ii  libwayland-egl1-mesa:amd64            10.3.2-1+deb8u1                      amd64        implementation of the Wayland EGL platform -- runtime
ii  libwayland-server0:amd64              1.6.0-2                              amd64        wayland compositor infrastructure - server library
ii  libwbclient0:amd64                    2:4.2.14+dfsg-0+deb8u2               amd64        Samba winbind client library
ii  libwebp5:amd64                        0.4.1-1.2+b2                         amd64        Lossy compression of digital photographic images.
ii  libwebrtc-audio-processing-0:amd64    0.1-3                                amd64        AudioProcessing module from the WebRTC project.
ii  libwinpr-crt0.1:amd64                 1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Windows Portable Runtime library (crt library)
ii  libwinpr-crypto0.1:amd64              1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Windows Portable Runtime library (crypto library)
ii  libwinpr-dsparse0.1:amd64             1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Windows Portable Runtime library (dsparse library)
ii  libwinpr-environment0.1:amd64         1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Windows Portable Runtime library (environment library)
ii  libwinpr-file0.1:amd64                1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Windows Portable Runtime library (file library)
ii  libwinpr-handle0.1:amd64              1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Windows Portable Runtime library (handle library)
ii  libwinpr-heap0.1:amd64                1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Windows Portable Runtime library (heap library)
ii  libwinpr-input0.1:amd64               1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Windows Portable Runtime library (input library)
ii  libwinpr-interlocked0.1:amd64         1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Windows Portable Runtime library (interlocked library)
ii  libwinpr-library0.1:amd64             1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Windows Portable Runtime library (library)
ii  libwinpr-path0.1:amd64                1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Windows Portable Runtime library (path library)
ii  libwinpr-pool0.1:amd64                1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Windows Portable Runtime library (pool library)
ii  libwinpr-registry0.1:amd64            1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Windows Portable Runtime library (registry library)
ii  libwinpr-rpc0.1:amd64                 1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Windows Portable Runtime library (RPC library)
ii  libwinpr-sspi0.1:amd64                1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Windows Portable Runtime library (sspi library)
ii  libwinpr-synch0.1:amd64               1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Windows Portable Runtime library (synch library)
ii  libwinpr-sysinfo0.1:amd64             1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Windows Portable Runtime library (sysinfo library)
ii  libwinpr-thread0.1:amd64              1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Windows Portable Runtime library (thread library)
ii  libwinpr-utils0.1:amd64               1.1.0~git20140921.1.440916e+dfsg1-4  amd64        Windows Portable Runtime library (utils library)
ii  libwmf0.2-7:amd64                     0.2.8.4-10.3+deb8u2                  amd64        Windows metafile conversion library
ii  libwnck-3-0:amd64                     3.4.9-3                              amd64        Window Navigator Construction Kit - runtime files
ii  libwnck-3-common                      3.4.9-3                              all          Window Navigator Construction Kit - common files
ii  libwnck-common                        2.30.7-2                             all          Window Navigator Construction Kit - common files
ii  libwnck22                             2.30.7-2                             amd64        Window Navigator Construction Kit - runtime files
ii  libwpd-0.10-10                        0.10.0-2+b1                          amd64        Library for handling WordPerfect documents (shared library)
ii  libwpg-0.3-3                          0.3.0-3                              amd64        WordPerfect graphics import/convert library (shared library)
ii  libwps-0.3-3                          0.3.0-2                              amd64        Works text file format import filter library (shared library)
ii  libwrap0:amd64                        7.6.q-25                             amd64        Wietse Venema's TCP wrappers library
ii  libwww-perl                           6.08-1                               all          simple and consistent interface to the world-wide web
ii  libwww-robotrules-perl                6.01-1                               all          database of robots.txt-derived permissions
ii  libx11-6:amd64                        2:1.6.2-3                            amd64        X11 client-side library
ii  libx11-6:i386                         2:1.6.2-3                            i386         X11 client-side library
ii  libx11-data                           2:1.6.2-3                            all          X11 client-side library
ii  libx11-dev:amd64                      2:1.6.2-3                            amd64        X11 client-side library (development headers)
ii  libx11-doc                            2:1.6.2-3                            all          X11 client-side library (development documentation)
ii  libx11-protocol-perl                  0.56-6                               all          Perl module for the X Window System Protocol, version 11
ii  libx11-xcb1:amd64                     2:1.6.2-3                            amd64        Xlib/XCB interface library
ii  libx264-142:amd64                     2:0.142.2431+gita5831aa-1+b2         amd64        x264 video coding library
ii  libxapian22                           1.2.19-1+deb8u1                      amd64        Search engine library
ii  libxatracker2:amd64                   10.3.2-1+deb8u1                      amd64        X acceleration library -- runtime
ii  libxau-dev:amd64                      1:1.0.8-1                            amd64        X11 authorisation library (development headers)
ii  libxau6:amd64                         1:1.0.8-1                            amd64        X11 authorisation library
ii  libxau6:i386                          1:1.0.8-1                            i386         X11 authorisation library
ii  libxaw7:amd64                         2:1.0.12-2+b1                        amd64        X11 Athena Widget library
ii  libxaw7:i386                          2:1.0.12-2+b1                        i386         X11 Athena Widget library
ii  libxcb-composite0:amd64               1.10-3+b1                            amd64        X C Binding, composite extension
ii  libxcb-dri2-0:amd64                   1.10-3+b1                            amd64        X C Binding, dri2 extension
ii  libxcb-dri3-0:amd64                   1.10-3+b1                            amd64        X C Binding, dri3 extension
ii  libxcb-glx0:amd64                     1.10-3+b1                            amd64        X C Binding, glx extension
ii  libxcb-keysyms1:amd64                 0.4.0-1                              amd64        utility libraries for X C Binding -- keysyms
ii  libxcb-present0:amd64                 1.10-3+b1                            amd64        X C Binding, present extension
ii  libxcb-randr0:amd64                   1.10-3+b1                            amd64        X C Binding, randr extension
ii  libxcb-render0:amd64                  1.10-3+b1                            amd64        X C Binding, render extension
ii  libxcb-render0:i386                   1.10-3+b1                            i386         X C Binding, render extension
ii  libxcb-shape0:amd64                   1.10-3+b1                            amd64        X C Binding, shape extension
ii  libxcb-shm0:amd64                     1.10-3+b1                            amd64        X C Binding, shm extension
ii  libxcb-shm0:i386                      1.10-3+b1                            i386         X C Binding, shm extension
ii  libxcb-sync1:amd64                    1.10-3+b1                            amd64        X C Binding, sync extension
ii  libxcb-util0:amd64                    0.3.8-3                              amd64        utility libraries for X C Binding -- atom, aux and event
ii  libxcb-xfixes0:amd64                  1.10-3+b1                            amd64        X C Binding, xfixes extension
ii  libxcb-xv0:amd64                      1.10-3+b1                            amd64        X C Binding, xv extension
ii  libxcb1:amd64                         1.10-3+b1                            amd64        X C Binding
ii  libxcb1:i386                          1.10-3+b1                            i386         X C Binding
ii  libxcb1-dev:amd64                     1.10-3+b1                            amd64        X C Binding, development files
ii  libxcomposite1:amd64                  1:0.4.4-1                            amd64        X11 Composite extension library
ii  libxcomposite1:i386                   1:0.4.4-1                            i386         X11 Composite extension library
ii  libxcursor1:amd64                     1:1.1.14-1+b1                        amd64        X cursor management library
ii  libxcursor1:i386                      1:1.1.14-1+b1                        i386         X cursor management library
ii  libxdamage1:amd64                     1:1.1.4-2+b1                         amd64        X11 damaged region extension library
ii  libxdamage1:i386                      1:1.1.4-2+b1                         i386         X11 damaged region extension library
ii  libxdmcp-dev:amd64                    1:1.1.1-1+b1                         amd64        X11 authorisation library (development headers)
ii  libxdmcp6:amd64                       1:1.1.1-1+b1                         amd64        X11 Display Manager Control Protocol library
ii  libxdmcp6:i386                        1:1.1.1-1+b1                         i386         X11 Display Manager Control Protocol library
ii  libxerces2-java                       2.11.0-7                             all          Validating XML parser for Java with DOM level 3 support
ii  libxext6:amd64                        2:1.3.3-1                            amd64        X11 miscellaneous extension library
ii  libxext6:i386                         2:1.3.3-1                            i386         X11 miscellaneous extension library
ii  libxfce4ui-1-0                        4.10.0-6                             amd64        widget library for Xfce
ii  libxfce4ui-utils                      4.10.0-6                             amd64        Utility files for libxfce4ui
ii  libxfce4util-bin                      4.10.1-2                             amd64        tools for libxfce4util
ii  libxfce4util-common                   4.10.1-2                             all          common files for libxfce4util
ii  libxfce4util6                         4.10.1-2                             amd64        Utility functions library for Xfce4
ii  libxfcegui4-4                         4.10.0-2                             amd64        Basic GUI C functions for Xfce4
ii  libxfconf-0-2                         4.10.0-3                             amd64        Client library for Xfce4 configure interface
ii  libxfixes3:amd64                      1:5.0.1-2+b2                         amd64        X11 miscellaneous 'fixes' extension library
ii  libxfixes3:i386                       1:5.0.1-2+b2                         i386         X11 miscellaneous 'fixes' extension library
ii  libxfont1:amd64                       1:1.5.1-1                            amd64        X11 font rasterisation library
ii  libxft2:amd64                         2.3.2-1                              amd64        FreeType-based font drawing library for X
ii  libxi6:amd64                          2:1.7.4-1+b2                         amd64        X11 Input extension library
ii  libxi6:i386                           2:1.7.4-1+b2                         i386         X11 Input extension library
ii  libxinerama1:amd64                    2:1.1.3-1+b1                         amd64        X11 Xinerama extension library
ii  libxinerama1:i386                     2:1.1.3-1+b1                         i386         X11 Xinerama extension library
ii  libxkbcommon0:amd64                   0.4.3-2                              amd64        library interface to the XKB compiler - shared library
ii  libxkbfile1:amd64                     1:1.0.8-1                            amd64        X11 keyboard file manipulation library
ii  libxklavier16                         5.2.1-1                              amd64        X Keyboard Extension high-level API
ii  libxml-commons-external-java          1.4.01-2                             all          XML Commons external code - DOM, SAX, and JAXP, etc
ii  libxml-commons-resolver1.1-java       1.2-7                                all          XML entity and URI resolver library
ii  libxml-parser-perl                    2.41-3                               amd64        Perl module for parsing XML files
ii  libxml-twig-perl                      1:3.48-1                             all          Perl module for processing huge XML documents in tree mode
ii  libxml-xpathengine-perl               0.13-1                               all          re-usable XPath engine for DOM-like trees
ii  libxml2:amd64                         2.9.1+dfsg1-5+deb8u4                 amd64        GNOME XML library
ii  libxml2:i386                          2.9.1+dfsg1-5+deb8u4                 i386         GNOME XML library
ii  libxmu6:amd64                         2:1.1.2-1                            amd64        X11 miscellaneous utility library
ii  libxmu6:i386                          2:1.1.2-1                            i386         X11 miscellaneous utility library
ii  libxmuu1:amd64                        2:1.1.2-1                            amd64        X11 miscellaneous micro-utility library
ii  libxom-java                           1.2.10-1                             all          New XML object model for Java
ii  libxpm4:amd64                         1:3.5.12-0+deb8u1                    amd64        X11 pixmap library
ii  libxpm4:i386                          1:3.5.12-0+deb8u1                    i386         X11 pixmap library
ii  libxpp2-java                          2.1.10-7                             all          XML pull parser library for java V2
ii  libxpp3-java                          1.1.4c-2                             all          XML pull parser library for java
ii  libxrandr2:amd64                      2:1.4.2-1+b1                         amd64        X11 RandR extension library
ii  libxrandr2:i386                       2:1.4.2-1+b1                         i386         X11 RandR extension library
ii  libxrender1:amd64                     1:0.9.8-1+b1                         amd64        X Rendering Extension client library
ii  libxrender1:i386                      1:0.9.8-1+b1                         i386         X Rendering Extension client library
ii  libxres1:amd64                        2:1.0.7-1+b1                         amd64        X11 Resource extension library
ii  libxshmfence1:amd64                   1.1-4                                amd64        X shared memory fences - shared library
ii  libxslt1.1:amd64                      1.1.28-2+deb8u2                      amd64        XSLT 1.0 processing library - runtime library
ii  libxslt1.1:i386                       1.1.28-2+deb8u2                      i386         XSLT 1.0 processing library - runtime library
ii  libxss1:amd64                         1:1.2.2-1                            amd64        X11 Screen Saver extension library
ii  libxt-dev:amd64                       1:1.1.4-1+b1                         amd64        X11 toolkit intrinsics library (development headers)
ii  libxt6:amd64                          1:1.1.4-1+b1                         amd64        X11 toolkit intrinsics library
ii  libxt6:i386                           1:1.1.4-1+b1                         i386         X11 toolkit intrinsics library
ii  libxtables10                          1.4.21-2+b1                          amd64        netfilter xtables library
ii  libxtst6:amd64                        2:1.2.2-1+b1                         amd64        X11 Testing -- Record extension library
ii  libxv1:amd64                          2:1.0.10-1+b1                        amd64        X11 Video extension library
ii  libxvidcore4:amd64                    2:1.3.3-1                            amd64        Open source MPEG-4 video codec (library)
ii  libxvmc1:amd64                        2:1.0.8-2+b1                         amd64        X11 Video extension library
ii  libxxf86dga1:amd64                    2:1.1.4-1+b1                         amd64        X11 Direct Graphics Access extension library
ii  libxxf86vm1:amd64                     1:1.1.3-1+b1                         amd64        X11 XFree86 video mode extension library
ii  libyajl2:amd64                        2.1.0-2                              amd64        Yet Another JSON Library
ii  libzvbi-common                        0.2.35-3                             all          Vertical Blanking Interval decoder (VBI) - common files
ii  libzvbi0:amd64                        0.2.35-3                             amd64        Vertical Blanking Interval decoder (VBI) - runtime files
ii  lightdm                               1.10.3-3                             amd64        simple display manager
ii  lightdm-gtk-greeter                   1.8.5-2                              amd64        simple display manager (GTK+ greeter)
ii  linux-base                            3.5                                  all          Linux image base package
ii  linux-compiler-gcc-4.8-x86            3.16.39-1+deb8u1                     amd64        Compiler for Linux on x86 (meta-package)
ii  linux-headers-3.16.0-4-amd64          3.16.39-1+deb8u1                     amd64        Header files for Linux 3.16.0-4-amd64
ii  linux-headers-3.16.0-4-common         3.16.39-1+deb8u1                     amd64        Common header files for Linux 3.16.0-4
ii  linux-headers-amd64                   3.16+63                              amd64        Header files for Linux amd64 configuration (meta-package)
ii  linux-image-3.16.0-4-amd64            3.16.39-1+deb8u1                     amd64        Linux 3.16 for 64-bit PCs
ii  linux-image-amd64                     3.16+63                              amd64        Linux for 64-bit PCs (meta-package)
ii  linux-kbuild-3.16                     3.16.7-ckt20-1                       amd64        Kbuild infrastructure for Linux 3.16
ii  linux-libc-dev:amd64                  3.16.39-1+deb8u1                     amd64        Linux support headers for userspace development
ii  live-boot                             4.0.2-1                              all          Live System Boot Components
ii  live-boot-doc                         4.0.2-1                              all          Live System Boot Components (documentation)
ii  live-boot-initramfs-tools             4.0.2-1                              all          Live System Boot Components (initramfs-tools backend)
ii  live-config                           4.0.4-1                              all          Live System Configuration Components
ii  live-config-doc                       4.0.4-1                              all          Live System Configuration Components (documentation)
ii  live-config-systemd                   4.0.4-1                              all          Live System Configuration Components (systemd backend)
ii  live-tools                            4.0.2-1.1                            all          Live System Extra Components
ii  lksctp-tools                          1.0.16+dfsg-2                        amd64        user-space access to Linux Kernel SCTP - commandline tools
ii  lm-sensors                            1:3.3.5-2                            amd64        utilities to read temperature/voltage/fan sensors
ii  locales                               2.19-18+deb8u7                       all          GNU C Library: National Language (locale) data [support]
ii  login                                 1:4.2-3+deb8u1                       amd64        system login tools
ii  logrotate                             3.8.7-1+b1                           amd64        Log rotation utility
ii  lp-solve                              5.5.0.13-7+b1                        amd64        Solve (mixed integer) linear programming problems
ii  lsb-base                              4.1+Debian13+nmu1                    all          Linux Standard Base 4.1 init script functionality
ii  lsb-release                           4.1+Debian13+nmu1                    all          Linux Standard Base version reporting utility
ii  m17n-db                               1.6.5-1                              all          multilingual text processing library - database
ii  make                                  4.0-8.1                              amd64        utility for directing compilation
ii  man-db                                2.7.0.2-5                            amd64        on-line manual pager
ii  manpages                              3.74-1                               all          Manual pages about using a GNU/Linux system
ii  manpages-dev                          3.74-1                               all          Manual pages about using GNU/Linux for development
ii  mathpiper                             0.81f+svn4469+dfsg3-3                all          Java Computer Algebra System
ii  mawk                                  1.3.3-17                             amd64        a pattern scanning and text processing language
ii  media-player-info                     22-2                                 all          Media player identification files
ii  mime-support                          3.58                                 all          MIME files 'mime.types' & 'mailcap', and support programs
ii  miscfiles                             1.4.2.dfsg.1-9.1                     all          Dictionaries and other interesting files
ii  mobile-broadband-provider-info        20140317-1                           all          database of mobile broadband service providers
ii  modemmanager                          1.4.0-1                              amd64        D-Bus service for managing modems
ii  mount                                 2.25.2-6                             amd64        Tools for mounting and manipulating filesystems
ii  mousepad                              0.3.0-2                              amd64        simple Xfce oriented text editor
ii  multiarch-support                     2.19-18+deb8u7                       amd64        Transitional package to ensure multiarch compatibility
ii  mythes-en-us                          1:3.3.0-4                            all          English Thesaurus for LibreOffice/OpenOffice.org
ii  nano                                  2.2.6-3                              amd64        small, friendly text editor inspired by Pico
ii  ncurses-base                          5.9+20140913-1                       all          basic terminal type definitions
ii  ncurses-bin                           5.9+20140913-1+b1                    amd64        terminal-related programs and man pages
ii  ncurses-term                          5.9+20140913-1                       all          additional terminal type definitions
ii  net-tools                             1.60-26+b1                           amd64        NET-3 networking toolkit
ii  netbase                               5.3                                  all          Basic TCP/IP networking system
ii  netcat-traditional                    1.10-41                              amd64        TCP/IP swiss army knife
ii  network-manager                       0.9.10.0-7                           amd64        network management framework (daemon and userspace tools)
ii  network-manager-gnome                 0.9.10.0-2                           amd64        network management framework (GNOME frontend)
ii  nfacct                                1.0.1-1.1                            amd64        netfilter accounting object tool
ii  notification-daemon                   0.7.6-2                              amd64        daemon for displaying passive pop-up notifications
ii  ntfs-3g                               1:2014.2.15AR.2-1+deb8u3             amd64        read/write NTFS driver for FUSE
ii  ocaml                                 4.01.0-5                             amd64        ML language implementation with a class-based object system
ii  ocaml-base                            4.01.0-5                             amd64        Runtime system for OCaml bytecode executables
ii  ocaml-base-nox                        4.01.0-5                             amd64        Runtime system for OCaml bytecode executables (no X)
ii  ocaml-compiler-libs                   4.01.0-5                             amd64        OCaml interpreter and standard libraries
ii  ocaml-interp                          4.01.0-5                             amd64        OCaml interactive interpreter and standard libraries
ii  ocaml-nox                             4.01.0-5                             amd64        ML implementation with a class-based object system (no X)
ii  opam                                  1.2.0-1+deb8u1                       amd64        package manager for OCaml
ii  opam-docs                             1.2.0-1+deb8u1                       all          package manager for OCaml (documentation)
ii  openjdk-7-jdk:amd64                   7u121-2.6.8-2~deb8u1                 amd64        OpenJDK Development Kit (JDK)
ii  openjdk-7-jre:amd64                   7u121-2.6.8-2~deb8u1                 amd64        OpenJDK Java runtime, using Hotspot JIT
ii  openjdk-7-jre-headless:amd64          7u121-2.6.8-2~deb8u1                 amd64        OpenJDK Java runtime, using Hotspot JIT (headless)
ii  openssh-client                        1:6.7p1-5+deb8u3                     amd64        secure shell (SSH) client, for secure access to remote machines
ii  openssh-server                        1:6.7p1-5+deb8u3                     amd64        secure shell (SSH) server, for secure access from remote machines
ii  openssh-sftp-server                   1:6.7p1-5+deb8u3                     amd64        secure shell (SSH) sftp server module, for SFTP access from remote machines
ii  openssl                               1.0.1t-1+deb8u6                      amd64        Secure Sockets Layer toolkit - cryptographic utility
ii  orage                                 4.10.0-1+b2                          amd64        Calendar for Xfce Desktop Environment
ii  p11-kit                               0.20.7-1                             amd64        p11-glue utilities
ii  p11-kit-modules:amd64                 0.20.7-1                             amd64        p11-glue proxy and trust modules
ii  p7zip-full                            9.20.1~dfsg.1-4.1+deb8u2             amd64        7z and 7za file archivers with high compression ratio
ii  packagekit                            1.0.1-2                              amd64        Provides a package management service
ii  packagekit-tools                      1.0.1-2                              amd64        Provides PackageKit command-line tools
ii  parted                                3.2-7                                amd64        disk partition manipulator
ii  passwd                                1:4.2-3+deb8u1                       amd64        change and administer password and group data
ii  patch                                 2.7.5-1                              amd64        Apply a diff file to an original
ii  perl                                  5.20.2-3+deb8u6                      amd64        Larry Wall's Practical Extraction and Report Language
ii  perl-base                             5.20.2-3+deb8u6                      amd64        minimal Perl system
ii  perl-modules                          5.20.2-3+deb8u6                      all          Core Perl modules
ii  policykit-1                           0.105-15~deb8u2                      amd64        framework for managing administrative policies and privileges
ii  policykit-1-gnome                     0.105-2                              amd64        GNOME authentication agent for PolicyKit-1
ii  poppler-data                          0.4.7-1                              all          encoding data for the poppler PDF rendering library
ii  ppp                                   2.4.6-3.1                            amd64        Point-to-Point Protocol (PPP) - daemon
ii  procps                                2:3.3.9-9                            amd64        /proc file system utilities
ii  psmisc                                22.21-2                              amd64        utilities that use the proc file system
ii  pulseaudio                            5.0-13                               amd64        PulseAudio sound server
ii  pulseaudio-module-x11                 5.0-13                               amd64        X11 module for PulseAudio sound server
ii  pulseaudio-utils                      5.0-13                               amd64        Command line tools for the PulseAudio sound server
ii  python                                2.7.9-1                              amd64        interactive high-level object-oriented language (default version)
ii  python-apt-common                     0.9.3.12                             all          Python interface to libapt-pkg (locales)
ii  python-cairo                          1.8.8-1+b2                           amd64        Python bindings for the Cairo vector graphics library
ii  python-cddb                           1.4-5.1+b3                           amd64        Python interface to CD-IDs and FreeDB
ii  python-chardet                        2.3.0-1                              all          universal character encoding detector for Python2
ii  python-cups                           1.9.63-1                             amd64        Python bindings for CUPS
ii  python-cupshelpers                    1.4.6-1                              all          Python utility modules around the CUPS printing system
ii  python-dbus                           1.2.0-2+b3                           amd64        simple interprocess messaging system (Python interface)
ii  python-dbus-dev                       1.2.0-2                              all          main loop integration development files for python-dbus
ii  python-feedparser                     5.1.3-3                              all          Universal Feed Parser for Python
ii  python-gi                             3.14.0-1                             amd64        Python 2.x bindings for gobject-introspection libraries
ii  python-gi-cairo                       3.14.0-1                             amd64        Python Cairo bindings for the GObject library
ii  python-gobject-2                      2.28.6-12+b1                         amd64        deprecated static Python bindings for the GObject library
ii  python-gtk2                           2.24.0-4                             amd64        Python bindings for the GTK+ widget set
ii  python-libxml2                        2.9.1+dfsg1-5+deb8u4                 amd64        Python bindings for the GNOME XML library
ii  python-minimal                        2.7.9-1                              amd64        minimal subset of the Python language (default version)
ii  python-musicbrainz2                   0.7.4-1                              all          interface to the MusicBrainz XML web service
ii  python-mutagen                        1.25.1-1                             all          audio metadata editing library
ii  python-numpy                          1:1.8.2-2                            amd64        Numerical Python adds a fast array facility to the Python language
ii  python-pkg-resources                  5.5.1-1                              all          Package Discovery and Resource Access using pkg_resources
ii  python-pycurl                         7.19.5-3                             amd64        Python bindings to libcurl
ii  python-pyinotify                      0.9.4-1                              all          simple Linux inotify Python bindings
ii  python-smbc                           1.0.15.3-0.1                         amd64        Python bindings for the Samba client library
ii  python-support                        1.0.15                               all          automated rebuilding support for Python modules
ii  python-talloc                         2.1.2-0+deb8u1                       amd64        hierarchical pool based memory allocator - Python bindings
ii  python-utidylib                       0.2-9                                all          Python wrapper for TidyLib
ii  python2.7                             2.7.9-2+deb8u1                       amd64        Interactive high-level object-oriented language (version 2.7)
ii  python2.7-minimal                     2.7.9-2+deb8u1                       amd64        Minimal subset of the Python language (version 2.7)
ii  python3                               3.4.2-2                              amd64        interactive high-level object-oriented language (default python3 version)
ii  python3-apt                           0.9.3.12                             amd64        Python 3 interface to libapt-pkg
ii  python3-brlapi                        5.2~20141018-5                       amd64        Braille display access via BRLTTY - Python3 bindings
ii  python3-cairo                         1.10.0+dfsg-4+b1                     amd64        Python 3 bindings for the Cairo vector graphics library
ii  python3-chardet                       2.3.0-1                              all          universal character encoding detector for Python3
ii  python3-debian                        0.1.27                               all          Python 3 modules to work with Debian-related data formats
ii  python3-gi                            3.14.0-1                             amd64        Python 3 bindings for gobject-introspection libraries
ii  python3-louis                         2.5.3-3                              all          Python bindings for liblouis
ii  python3-minimal                       3.4.2-2                              amd64        minimal subset of the Python language (default python3 version)
ii  python3-pkg-resources                 5.5.1-1                              all          Package Discovery and Resource Access using pkg_resources
ii  python3-pyatspi                       2.14.0+dfsg-1                        all          Assistive Technology Service Provider Interface - Python3 bindings
ii  python3-six                           1.8.0-1                              all          Python 2 and 3 compatibility library (Python 3 interface)
ii  python3-speechd                       0.8-7                                all          Python interface to Speech Dispatcher
ii  python3-uno                           1:4.3.3-2+deb8u5                     amd64        Python-UNO bridge
ii  python3-xdg                           0.25-4                               all          Python 3 library to access freedesktop.org standards
ii  python3.4                             3.4.2-1                              amd64        Interactive high-level object-oriented language (version 3.4)
ii  python3.4-minimal                     3.4.2-1                              amd64        Minimal subset of the Python language (version 3.4)
ii  qdbus                                 4:4.8.6+git64-g5dc8b2b+dfsg-3+deb8u1 amd64        Qt 4 D-Bus tool
ii  qt-at-spi:amd64                       0.3.1-5                              amd64        at-spi accessibility plugin for Qt
ii  qtchooser                             47-gd2b7997-2                        amd64        Wrapper to select between Qt development binary versions
ii  qtcore4-l10n                          4:4.8.6+git64-g5dc8b2b+dfsg-3+deb8u1 all          Qt 4 core module translations
ii  quodlibet                             3.2.2-1                              all          audio library manager and player for GTK3
ii  rarian-compat                         0.8.1-6                              amd64        Documentation meta-data library (compatibility tools)
ii  readline-common                       6.3-8                                all          GNU readline and history libraries, common files
ii  rename                                0.20-3                               all          Perl extension for renaming multiple files
ii  ristretto                             0.6.3-2                              amd64        lightweight picture-viewer for the Xfce desktop environment
ii  rlwrap                                0.41-1                               amd64        readline feature command line wrapper
ii  rsync                                 3.1.1-3                              amd64        fast, versatile, remote (and local) file-copying tool
ii  rsyslog                               8.4.2-1+deb8u2                       amd64        reliable system and kernel logging daemon
ii  rtkit                                 0.11-2                               amd64        Realtime Policy and Watchdog Daemon
ii  samba-libs:amd64                      2:4.2.14+dfsg-0+deb8u2               amd64        Samba core libraries
ii  sane-utils                            1.0.24-8+deb8u1                      amd64        API library for scanners -- utilities
ii  sed                                   4.2.2-4+deb8u1                       amd64        The GNU sed stream editor
ii  sensible-utils                        0.0.9                                all          Utilities for sensible alternative selection
ii  sgml-base                             1.26+nmu4                            all          SGML infrastructure and SGML catalog file support
ii  sgml-data                             2.0.10                               all          common SGML and XML data
ii  shared-mime-info                      1.3-1                                amd64        FreeDesktop.org shared MIME database and spec
ii  speech-dispatcher                     0.8-7                                amd64        Common interface to speech synthesizers
ii  speech-dispatcher-audio-plugins:amd64 0.8-7                                amd64        Speech Dispatcher: Audio output plugins
ii  startpar                              0.59-3                               amd64        run processes in parallel and multiplex their output
ii  sudo                                  1.8.10p3-1+deb8u3                    amd64        Provide limited super user privileges to specific users
ii  synaptic                              0.81.2                               amd64        Graphical package manager
ii  system-config-printer                 1.4.6-1                              all          graphical interface to configure the printing system
ii  system-config-printer-udev            1.4.6-1                              amd64        Utilities to detect and configure printers automatically
ii  systemd                               215-17+deb8u6                        amd64        system and service manager
ii  systemd-sysv                          215-17+deb8u6                        amd64        system and service manager - SysV links
ii  sysv-rc                               2.88dsf-59                           all          System-V-like runlevel change mechanism
ii  sysvinit-utils                        2.88dsf-59                           amd64        System-V-like utilities
ii  tango-icon-theme                      0.8.90-5                             all          Tango icon theme
ii  tar                                   1.27.1-2+deb8u1                      amd64        GNU version of the tar archiving utility
ii  task-desktop                          3.31+deb8u1                          all          Debian desktop environment
ii  task-xfce-desktop                     3.31+deb8u1                          all          Xfce
ii  tasksel                               3.31+deb8u1                          all          tool for selecting tasks for installation on Debian systems
ii  tasksel-data                          3.31+deb8u1                          all          official tasks used for installation of Debian systems
ii  tcl                                   8.6.0+8                              amd64        Tool Command Language (default version) - shell
ii  tcl8.6                                8.6.2+dfsg-2                         amd64        Tcl (the Tool Command Language) v8.6 - shell
ii  tcpd                                  7.6.q-25                             amd64        Wietse Venema's TCP wrapper utilities
ii  thunar                                1.6.3-2                              amd64        File Manager for Xfce
ii  thunar-archive-plugin                 0.3.1-3                              amd64        Archive plugin for Thunar file manager
ii  thunar-data                           1.6.3-2                              all          Provides thunar documentation, icons and translations
ii  thunar-media-tags-plugin              0.2.1-1                              amd64        Media tags plugin for Thunar file manager
ii  thunar-volman                         0.8.0-4                              amd64        Thunar extension for volumes management
ii  tk                                    8.6.0+8                              amd64        Toolkit for Tcl and X11 (default version) - windowing shell
ii  tk8.6                                 8.6.2-1                              amd64        Tk toolkit for Tcl and X11 v8.6 - windowing shell
ii  traceroute                            1:2.0.20-2+b1                        amd64        Traces the route taken by packets over an IPv4/IPv6 network
ii  tumbler                               0.1.30-1+b1                          amd64        D-Bus thumbnailing service
ii  tumbler-common                        0.1.30-1                             all          D-Bus thumbnailing service (common files)
ii  tzdata                                2016j-0+deb8u1                       all          time zone and daylight-saving time data
ii  tzdata-java                           2016j-0+deb8u1                       all          time zone and daylight-saving time data for use by java runtimes
ii  ucf                                   3.0030                               all          Update Configuration File(s): preserve user changes to config files
ii  udev                                  215-17+deb8u6                        amd64        /dev/ and hotplug management daemon
ii  udisks2                               2.1.3-5                              amd64        D-Bus service to access and manipulate storage devices
ii  uno-libs3                             4.3.3-2+deb8u5                       amd64        LibreOffice UNO runtime environment -- public shared libraries
ii  unzip                                 6.0-16+deb8u2                        amd64        De-archiver for .zip files
ii  update-inetd                          4.43                                 all          inetd configuration file updater
ii  upower                                0.99.1-3.2                           amd64        abstraction for power management
ii  ure                                   4.3.3-2+deb8u5                       amd64        LibreOffice UNO runtime environment
ii  usb-modeswitch                        2.2.0+repack0-2                      amd64        mode switching tool for controlling "flip flop" USB devices
ii  usb-modeswitch-data                   20150115-1                           all          mode switching data for usb-modeswitch
ii  usbmuxd                               1.0.8+git20140527.e72f2f7-2          amd64        USB multiplexor daemon for iPhone and iPod Touch devices
ii  user-setup                            1.61                                 all          Set up initial user and password
ii  util-linux                            2.25.2-6                             amd64        Miscellaneous system utilities
ii  uuid-runtime                          2.25.2-6                             amd64        runtime components for the Universally Unique ID library
ii  va-driver-all:amd64                   1.4.1-1                              amd64        Video Acceleration (VA) API -- driver metapackage
ii  vdpau-va-driver:amd64                 0.7.4-3                              amd64        VDPAU-based backend for VA API
ii  vim                                   2:7.4.488-7+deb8u2                   amd64        Vi IMproved - enhanced vi editor
ii  vim-common                            2:7.4.488-7+deb8u2                   amd64        Vi IMproved - Common files
ii  vim-runtime                           2:7.4.488-7+deb8u2                   all          Vi IMproved - Runtime files
ii  vim-tiny                              2:7.4.488-7+deb8u2                   amd64        Vi IMproved - enhanced vi editor - compact version
ii  vlc                                   2.2.4-1~deb8u1                       amd64        multimedia player and streamer
ii  vlc-data                              2.2.4-1~deb8u1                       all          Common data for VLC
ii  vlc-nox                               2.2.4-1~deb8u1                       amd64        multimedia player and streamer (without X support)
ii  vlc-plugin-notify                     2.2.4-1~deb8u1                       amd64        LibNotify plugin for VLC
ii  vlc-plugin-samba                      2.2.4-1~deb8u1                       amd64        Samba plugin for VLC
ii  wget                                  1.16-1+deb8u1                        amd64        retrieves files from the web
ii  whiptail                              0.52.17-1+b1                         amd64        Displays user-friendly dialog boxes from shell scripts
ii  wireless-regdb                        2016.06.10-1~deb8u1                  all          wireless regulatory database
ii  wireless-tools                        30~pre9-8                            amd64        Tools for manipulating Linux Wireless Extensions
ii  wpasupplicant                         2.3-1+deb8u4                         amd64        client support for WPA and WPA2 (IEEE 802.11i)
ii  x11-apps                              7.7+4                                amd64        X applications
ii  x11-common                            1:7.7+7                              all          X Window System (X.Org) infrastructure
ii  x11-session-utils                     7.7+1                                amd64        X session utilities
ii  x11-utils                             7.7+2                                amd64        X11 utilities
ii  x11-xkb-utils                         7.7+1                                amd64        X11 XKB utilities
ii  x11-xserver-utils                     7.7+3+b1                             amd64        X server utilities
ii  x11proto-core-dev                     7.0.26-1                             all          X11 core wire protocol and auxiliary headers
ii  x11proto-input-dev                    2.3.1-1                              all          X11 Input extension wire protocol
ii  x11proto-kb-dev                       1.0.6-2                              all          X11 XKB extension wire protocol
ii  xarchiver                             1:0.5.4-1+deb8u1                     amd64        GTK+ frontend for most used compression formats
ii  xauth                                 1:1.0.9-1                            amd64        X authentication utility
ii  xbitmaps                              1.1.1-2                              all          Base X bitmaps
ii  xbrlapi                               5.2~20141018-5                       amd64        Access software for a blind person using a braille display - xbrlapi
ii  xdg-user-dirs                         0.15-2                               amd64        tool to manage well known user directories
ii  xdg-utils                             1.1.0~rc1+git20111210-7.4            all          desktop integration utilities from freedesktop.org
ii  xfburn                                0.5.2-1                              amd64        CD-burner application for Xfce Desktop Environment
ii  xfce-keyboard-shortcuts               4.10.0-6                             all          xfce keyboard shortcuts configuration
ii  xfce4                                 4.10.1                               all          Meta-package for the Xfce Lightweight Desktop Environment
ii  xfce4-appfinder                       4.10.1-1                             amd64        Application finder for the Xfce4 Desktop Environment
ii  xfce4-artwork                         0.1.1a~git+20110420-1                all          additional artwork for the Xfce4 Desktop Environment
ii  xfce4-battery-plugin                  1.0.5-4                              amd64        battery monitor plugin for the Xfce4 panel
ii  xfce4-clipman                         2:1.2.6-1                            amd64        clipboard history utility
ii  xfce4-clipman-plugin                  2:1.2.6-1                            amd64        clipboard history plugin for Xfce panel
ii  xfce4-cpufreq-plugin                  1.1.0-1                              amd64        cpufreq information plugin for the Xfce4 panel
ii  xfce4-cpugraph-plugin                 1.0.5-1                              amd64        CPU load graph plugin for the Xfce4 panel
ii  xfce4-datetime-plugin                 0.6.2-2                              amd64        date and time plugin for the Xfce4 panel
ii  xfce4-dict                            0.7.0-1                              amd64        Dictionary plugin for Xfce4 panel
ii  xfce4-diskperf-plugin                 2.5.4-3                              amd64        disk performance display plugin for the Xfce4 panel
ii  xfce4-fsguard-plugin                  1.0.1-2                              amd64        filesystem monitor plugin for the Xfce4 panel
ii  xfce4-genmon-plugin                   3.4.0-2                              amd64        Generic Monitor for the Xfce4 panel
ii  xfce4-goodies                         4.10                                 amd64        enhancements for the Xfce4 Desktop Environment
ii  xfce4-mailwatch-plugin                1.2.0-2                              amd64        mail watcher plugin for the Xfce4 panel
ii  xfce4-mixer                           4.10.0-3                             amd64        Xfce mixer application
ii  xfce4-mount-plugin                    0.6.7-1                              amd64        mount plugin for the Xfce4 panel
ii  xfce4-netload-plugin                  1.2.0-1                              amd64        network load monitor plugin for the Xfce4 panel
ii  xfce4-notes                           1.7.7-5                              amd64        Notes application for the Xfce4 desktop
ii  xfce4-notes-plugin                    1.7.7-5                              amd64        Notes plugin for the Xfce4 desktop
ii  xfce4-notifyd                         0.2.4-3                              amd64        simple, visually-appealing notification daemon for Xfce
ii  xfce4-panel                           4.10.1-1                             amd64        panel for Xfce4 desktop environment
ii  xfce4-places-plugin                   1.6.0-1                              amd64        quick access to folders, documents and removable media
ii  xfce4-power-manager                   1.4.1-1                              amd64        power manager for Xfce desktop
ii  xfce4-power-manager-data              1.4.1-1                              all          power manager for Xfce desktop, arch-indep files
ii  xfce4-power-manager-plugins           1.4.1-1                              amd64        power manager plugins for Xfce panel
ii  xfce4-quicklauncher-plugin            1.9.4-11                             amd64        rapid launcher plugin for the Xfce4 panel
ii  xfce4-screenshooter                   1.8.1-5                              amd64        screenshots utility for Xfce
ii  xfce4-sensors-plugin                  1.2.5-2                              amd64        hardware sensors plugin for the Xfce4 panel
ii  xfce4-session                         4.10.1-10                            amd64        Xfce4 Session Manager
ii  xfce4-settings                        4.10.1-2                             amd64        graphical application for managing Xfce settings
ii  xfce4-smartbookmark-plugin            0.4.5-1                              amd64        search the web via the Xfce4 panel
ii  xfce4-systemload-plugin               1.1.1-4                              amd64        system load monitor plugin for the Xfce4 panel
ii  xfce4-taskmanager                     1.0.1-1                              amd64        process manager for the Xfce4 Desktop Environment
ii  xfce4-terminal                        0.6.3-1+b1                           amd64        Xfce terminal emulator
ii  xfce4-timer-plugin                    1.6.0-1                              amd64        timer plugin for Xfce panel
ii  xfce4-verve-plugin                    1.0.0-3                              amd64        Verve (command line) plugin for Xfce panel
ii  xfce4-volumed                         0.1.13-5                             amd64        volume keys daemon
ii  xfce4-wavelan-plugin                  0.5.11-3                             amd64        wavelan status plugin for the Xfce4 panel
ii  xfce4-weather-plugin                  0.8.3-2                              amd64        weather information plugin for the Xfce4 panel
ii  xfce4-xkb-plugin                      1:0.5.6-1                            amd64        xkb layout switch plugin for the Xfce4 panel
ii  xfconf                                4.10.0-3                             amd64        utilities for managing settings in Xfce
ii  xfdesktop4                            4.10.2-3                             amd64        xfce desktop background, icons and root menu manager
ii  xfdesktop4-data                       4.10.2-3                             all          xfce desktop background, icons and root menu (common files)
ii  xfonts-100dpi                         1:1.0.3                              all          100 dpi fonts for X
ii  xfonts-75dpi                          1:1.0.3                              all          75 dpi fonts for X
ii  xfonts-base                           1:1.0.3                              all          standard fonts for X
ii  xfonts-encodings                      1:1.0.4-2                            all          Encodings for X.Org fonts
ii  xfonts-scalable                       1:1.0.3-1                            all          scalable fonts for X
ii  xfonts-utils                          1:7.7+2                              amd64        X Window System font utility programs
ii  xfwm4                                 4.10.1-3                             amd64        window manager of the Xfce project
ii  xinit                                 1.3.4-1                              amd64        X server initialisation tool
ii  xkb-data                              2.12-1                               all          X Keyboard Extension (XKB) configuration data
ii  xml-core                              0.13+nmu2                            all          XML infrastructure and XML catalog file support
ii  xorg                                  1:7.7+7                              amd64        X.Org X Window System
ii  xorg-docs-core                        1:1.7-1                              all          Core documentation for the X.org X Window System
ii  xorg-sgml-doctools                    1:1.11-1                             all          Common tools for building X.Org SGML documentation
ii  xsane                                 0.998-6+b1                           amd64        featureful graphical frontend for SANE (Scanner Access Now Easy)
ii  xsane-common                          0.998-6                              all          featureful graphical frontend for SANE (Scanner Access Now Easy)
ii  xscreensaver                          5.30-1+deb8u2                        amd64        Screensaver daemon and frontend for X11
ii  xscreensaver-data                     5.30-1+deb8u2                        amd64        Screen saver modules for screensaver frontends
ii  xserver-common                        2:1.16.4-1                           all          common files used by various X servers
ii  xserver-xorg                          1:7.7+7                              amd64        X.Org X server
ii  xserver-xorg-core                     2:1.16.4-1                           amd64        Xorg X server - core server
ii  xserver-xorg-input-all                1:7.7+7                              amd64        X.Org X server -- input driver metapackage
ii  xserver-xorg-input-evdev              1:2.9.0-2                            amd64        X.Org X server -- evdev input driver
ii  xserver-xorg-input-mouse              1:1.9.1-1                            amd64        X.Org X server -- mouse input driver
ii  xserver-xorg-input-synaptics          1.8.1-1                              amd64        Synaptics TouchPad driver for X.Org server
ii  xserver-xorg-input-vmmouse            1:13.0.0-1+b3                        amd64        X.Org X server -- VMMouse input driver to use with VMWare
ii  xserver-xorg-input-wacom              0.26.0+20140918-1                    amd64        X.Org X server -- Wacom input driver
ii  xserver-xorg-video-all                1:7.7+7                              amd64        X.Org X server -- output driver metapackage
ii  xserver-xorg-video-ati                1:7.5.0-1                            amd64        X.Org X server -- AMD/ATI display driver wrapper
ii  xserver-xorg-video-cirrus             1:1.5.2-2+b1                         amd64        X.Org X server -- Cirrus display driver
ii  xserver-xorg-video-fbdev              1:0.4.4-1+b2                         amd64        X.Org X server -- fbdev display driver
ii  xserver-xorg-video-intel              2:2.21.15-2+b2                       amd64        X.Org X server -- Intel i8xx, i9xx display driver
ii  xserver-xorg-video-mach64             6.9.4-2                              amd64        X.Org X server -- ATI Mach64 display driver
ii  xserver-xorg-video-mga                1:1.6.3-2+b1                         amd64        X.Org X server -- MGA display driver
ii  xserver-xorg-video-modesetting        0.9.0-2                              amd64        X.Org X server -- Generic modesetting driver
ii  xserver-xorg-video-neomagic           1:1.2.8-1+b2                         amd64        X.Org X server -- Neomagic display driver
ii  xserver-xorg-video-nouveau            1:1.0.11-1                           amd64        X.Org X server -- Nouveau display driver
ii  xserver-xorg-video-openchrome         1:0.3.3-1+b2                         amd64        X.Org X server -- VIA display driver
ii  xserver-xorg-video-qxl                0.1.1-2+b1                           amd64        X.Org X server -- QXL display driver
ii  xserver-xorg-video-r128               6.9.2-1+b2                           amd64        X.Org X server -- ATI r128 display driver
ii  xserver-xorg-video-radeon             1:7.5.0-1                            amd64        X.Org X server -- AMD/ATI Radeon display driver
ii  xserver-xorg-video-savage             1:2.3.7-2+b2                         amd64        X.Org X server -- Savage display driver
ii  xserver-xorg-video-siliconmotion      1:1.7.7-2+b2                         amd64        X.Org X server -- SiliconMotion display driver
ii  xserver-xorg-video-sisusb             1:0.9.6-2+b2                         amd64        X.Org X server -- SiS USB display driver
ii  xserver-xorg-video-tdfx               1:1.4.5-1+b2                         amd64        X.Org X server -- tdfx display driver
ii  xserver-xorg-video-trident            1:1.3.6-2+b2                         amd64        X.Org X server -- Trident display driver
ii  xserver-xorg-video-vesa               1:2.3.3-1+b3                         amd64        X.Org X server -- VESA display driver
ii  xserver-xorg-video-vmware             1:13.0.2-3.1                         amd64        X.Org X server -- VMware display driver
ii  xterm                                 312-2                                amd64        X terminal emulator
ii  xtrans-dev                            1.3.4-1                              all          X transport library (development files)
ii  xz-utils                              5.1.1alpha+20120614-2+b3             amd64        XZ-format compression utilities
ii  zlib1g:amd64                          1:1.2.8.dfsg-2+b1                    amd64        compression library - runtime
ii  zlib1g:i386                           1:1.2.8.dfsg-2+b1                    i386         compression library - runtime
```
