---
date: 2016-03-09T00:11:02+01:00
title: Caractéristiques
weight: 10
---

## Système de base

  Debian 8 (jessie) avec le gestionnaire de fenêtre XFCE.


## Utilisateur

Un utilisateur est créé par défaut : 

    utilisateur: candidat
    mot de passe : capes


## Liste des principaux logiciels

    geogebra
    libreoffice
    gnome-paint
    opam
    vim
    emacs
    rlwrap
    geany
    openjdk-7-jdk
    g++
    git
    scilab* [ 6.0.0-beta2 avec le module lycée 1.4.2 ]
    scratch 2* [v 453]

- La liste complète des paquets logiciels est disponible [ici](dpkg-l)
- * installés hors système de paquets

## Persistance des données

Par défaut une clé USB amorçable écrit les données dans la mémoire vive de
l'ordinateur hôte. Cela a pour bénéfice d'être rapide et de ne pas user le
support trop rapidement. Mais cela signifie que toutes les données créées au
cours d'une session seront perdues lors de l'extinction de la machine. 

* La clé CAPES offre la persistance des données utilisateurs.
Concrètement tout ce qui sera écrit dans le `home` de l'utilisateur sera
conservé.


## Support Wifi

La distribution supporte un certain nombre de cartes wifi par défaut : 

* [ralink](https://packages.debian.org/fr/jessie/firmware-ralink)
* [realtek](https://packages.debian.org/fr/jessie/firmware-realtek)
* [intel](https://packages.debian.org/fr/jessie/firmware-iwlwifi)
* [atheros](https://packages.debian.org/fr/jessie/firmware-atheros)
* [marvell libertas](https://packages.debian.org/fr/jessie/firmware-libertas)
* [broadcom](https://packages.debian.org/fr/jessie/firmware-brcm80211) et [d'autres brodcom (macbook pro)](https://wiki.debian.org/fr/wl)

## Clavier

Le clavier par défaut est un clavier FR 105 standard. Il est configurable depuis les menus une fois le système démarré.
