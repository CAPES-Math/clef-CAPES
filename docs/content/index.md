---
date: 2016-03-08T21:07:13+01:00
title: Introduction et Téléchargement
type: index
weight: 0
---

## Presentation

La clé CAPES est un environnment Linux amorçable sur votre PC ou dans une
machine virtuelle. Il permet au candidat de s'entraîner dans les conditions du
concours tout au long de l'année. 

Depuis cette page vous pouvez : 

* télécharger une image, la copier sur une clé USB ou l'utiliser comme machine virtuelle
* voir les [caractéristiques]({{< relref "caracteristiques/index.md" >}})
* prendre contact avec la [communauté]({{< relref "communaute/index.md" >}})

## Téléchargement

### Pour clé usb 

Une clé d'au moins 4 Go est nécessaire. 

* Télécharger le [zip](/downloads/) de l'image puis suivre les [instructions d'installation]({{< relref "installation/index.md" >}}).

### Pour machine virtuelle 

* VirtualBox : [vmdk](/downloads/) (1.8 Go)
