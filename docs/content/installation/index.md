---
date: 2016-04-11T00:23:02+01:00
title: Installation
weight: 30
---

## Copier l'image sur une clé usb

1. Décompresser le fichier [zip](/#téléchargement). Vous devez disposer de 4GO d'espace disponible avant cette opération.

2. Copier le fichier iso obtenu sur un clé usb.  
Pour cette opération vous pouvez utiliser le logiciel [etcher](https://etcher.io/).

3. Insérer la clé dans le lecteur usb de l'ordinateur voulu et rebooter le.

> Le boot via usb est une option qui doit être activée au niveau du firmware de
l'ordinateur.
 
> Pour l'instant la clé ne présente pas de support pour le boot UEFI. Il est
possible que vous ayez à le désactiver dans le BIOS pour faire fonctionner la
clé. Une solution alternative consiste à utiliser la clé en tant que machine
virtuelle sur votre système usuel.

## Augmenter la taille de la partition de persistance

Pour augmenter la taille du `home`, vous pouvez étendre la taille de la seconde
partition présente sur la clé. Il est possible de le faire depuis votre machine
locale (celle depuis laquelle l'image de la clé a été copiée) en ayant la clé
branchée à un port usb.

Il est également possible de le faire depuis le système présent sur la clé : 

1. Démarrer un ordinateur depuis votre clé usb fraichement installée

2. Au menu du démarrage, choisir avec les flèches le second menu : `Live (non
   persistant)`. Cela aura pour effet de ne pas utiliser le mode persistant et
   donc la partition associée non plus.

3. Démarrer l'utilitaire gparted en tapant : `sudo gparted` dans le terminal.

4. Sélectionner la clé usb (par exemple `/dev/sdb`). Vérifier qu'il s'agit bien
   de la clé usb. Par exemple il doit y avoir 2 partitions et la seconde doit
   avoir un label `persistence` associée.

5. À l'aide de l'outil, augmenter la taille de la seconde partition, puis
   valider.

6. Quitter `gparted` et rebooter votre ordinateur. Au menu du démarrage, vous
   pouvez sélectionner le premier mode. Vous diposerez de la persistance de
   données et la taille qui y est affecté sera celle choisie à l'étape
   précédente.
