#!/usr/bin/env bash

set -x
set -e

if [ -z "$@" ]
then
  opts="--provider=virtualbox"
else
  opts="$@"
fi
vagrant up build $opts

# putting this there while waiting the repo to be public
vagrant ssh build -c 'sudo /vagrant/scripts/bootstrap.sh'

# building the iso
vagrant ssh build -c 'sudo /vagrant/scripts/build.sh'

# post_processe the files
vagrant ssh build -c 'sudo /vagrant/scripts/post_process.sh'

# Shutdown the VM
vagrant halt build
