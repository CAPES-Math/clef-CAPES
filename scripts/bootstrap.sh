#!/usr/bin/env bash

set -x
set -e

apt-get update

# custom deps
apt-get install -y\
  git\
  qemu-utils\
  zip\
  python-crypto\
  python-yaml\
  dosfstools\
  kpartx

# Dependencies for live-build
apt-get install -y $(apt-cache depends live-build | grep 'Depends' | awk '{print $2}')

# Used when adding the persistence
apt-get install -y parted

# We assume live-build is installed on /vagrant/live-build
DEST=/vagrant/live-build
if [ -d "$DEST" ]
then
  echo "Don't clone again"
else
  git clone https://gitlab.inria.fr/CAPES-Math/live-build.git $DEST
fi
cd $DEST && make install


