#!/usr/bin/env bash

# Extract the users created following the strucuture :
#
#    build-output/
#       users/
#         user1/
#           .ssh/
#             id_rsa
#             id_rsa.pub
#         user2/
#           .ssh/
#             id_rsa
#             id_rsa.pub
#         user3/
#         ...
#
# Creates a yml file containining the list of user and their pub key.
# This file can be used to bootsrap the gitlab installation (see infra project)

set -x
set -e
set -u


SCRIPT_DIR=$(cd "$(dirname "$0")" && pwd)
BASE_DIR=$SCRIPT_DIR/..
SETTINGS_FILE=$BASE_DIR/settings.sh

source $SETTINGS_FILE

homes=$PROJECT_DIR/chroot/home
dest_output_users=/vagrant/build-output/users
dest_output_yaml=$dest_output_users/../users.yaml


#for i in $(ls $homes)
#do
#  dest=$dest_output_users/$(basename $i)
#  mkdir -p  $dest
#  cp -r $homes/$i/.ssh $dest/.
#done

echo "---" > $dest_output_yaml
echo "users:" >> $dest_output_yaml

for i in $(ls $dest_output_users)
do
  echo "  { login: $i, pub_key: \"$(cat $dest_output_users/$i/.ssh/id_rsa.pub)\"}" >> $dest_output_yaml
done
