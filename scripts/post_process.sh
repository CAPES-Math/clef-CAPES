#!/usr/bin/env bash

set -x
set -e


trap cleanup INT TERM EXIT

cleanup(){
  echo "Cleaning the loop devices"
  losetup -D
}


SCRIPT_DIR=$(cd "$(dirname "$0")" && pwd)
BASE_DIR=$SCRIPT_DIR/..
SETTINGS_FILE=$BASE_DIR/settings.sh

source $SETTINGS_FILE
source $SCRIPT_DIR/functions.sh

BUILD_DIR=$PROJECT_DIR/isos

# Move to the project dir
cd $PROJECT_DIR

# Make the isos directory
rm -rf $BUILD_DIR
mkdir -p $BUILD_DIR

# Get the iso generated by live-build
ORIG=( $(ls *.iso) )
ORIG=${ORIG[0]}
# size in bytes
SIZE_B=$(($SIZE * 1024 * 1024 * 1024))

# Name of the destination file
date=$(date +%Y-%m-%d_%H-%M-%S)
output=cle-capes-${ARCHITECTURE}-${SIZE}G-$VERSION-$date-persist
orig=cle-capes-${ARCHITECTURE}-$VERSION-$date-non-persist
RAW=$BUILD_DIR/$output.iso

# Create the raw disk that will be filled with the orig image
dd if=/dev/null of=$RAW bs=1 count=0 seek=${SIZE}G
free_loop=$(losetup -f)
losetup -o 0 $free_loop $RAW
# Copy the orig image
dd if=$ORIG of=$free_loop bs=4M
losetup -D

# Create the contextualization partition
# This will be used by cloud init when the key is booted
# Create the persistence partition
start=$(create_part $RAW 65)
free_loop=$(losetup -f)
losetup -o $start $free_loop $RAW
mkfs.vfat -I -n cidata $free_loop
losetup -D

# Create the persistence partition
start=$(create_part $RAW 100)
free_loop=$(losetup -f)
losetup -o $start $free_loop $RAW
mkfs.ext4 -F -L persistence $free_loop

mkdir -p /mnt/persistence
mount $free_loop /mnt/persistence

echo "/home union" > /mnt/persistence/persistence.conf

umount /mnt/persistence
losetup -D

cp $RAW  /vagrant/$output.iso
if [ "$USE_CONTEXT" == "true" ]
then
  copy_directory_in /vagrant/$output.iso cidata /vagrant/context
fi
# convert all the images
qemu-img convert -O vmdk /vagrant/$output.iso /vagrant/$output.vmdk
qemu-img convert -O qcow2 /vagrant/$output.iso /vagrant/$output.qcow2
cp $ORIG  /vagrant/$orig.iso
zip -jD /vagrant/$output.zip $RAW
