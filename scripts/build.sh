#!/usr/bin/env bash

set -x
set -e

SCRIPT_DIR=$(cd "$(dirname "$0")" && pwd)
BASE_DIR=$SCRIPT_DIR/..
SETTINGS_FILE=$BASE_DIR/settings.sh

source $SETTINGS_FILE
LB_CONFIG_DIR_BASE=$BASE_DIR/$CONFIG_SRC

# Cleaning previous installation
rm -rf $PROJECT_DIR
mkdir -p $PROJECT_DIR && cd $PROJECT_DIR

lb config\
  --architectures $ARCHITECTURE\
  --binary-images iso-hybrid\
  --bootappend-live "boot=live components quiet splash persistence"\
  --distribution jessie\
  --mode debian

## Copying extra configuration directory
cp -r $LB_CONFIG_DIR_BASE/* config/.

GENERATED_USERS_FILE="$BASE_DIR/users-generated.yaml"
# Allow to reuse a previously generated file
if [ ! -e $GENERATED_USERS_FILE ]
then
  python $SCRIPT_DIR/users-generator.py $BASE_DIR
fi
# We copy the users file in the ansible environment
# This file will be used as input by the ansible recipe
# It will overwrite the default users set
if [ -e $GENERATED_USERS_FILE ]
then
    cp $GENERATED_USERS_FILE config/includes.chroot/root/provisioning/.
fi

# Building
lb build
