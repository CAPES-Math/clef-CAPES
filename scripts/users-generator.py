from Crypto.PublicKey import RSA
import os
import sys
import yaml

"""
Generate the ssh_keys for the each user
Store the result in a users-genrated.yaml file
This file can be used
- in the key building process
- in the infra building process
"""

if len(sys.argv) != 2:
    print("Wrong number of arguments")
    sys.exit(1)

base_dir = sys.argv[1]

user_file = os.path.join(base_dir, 'users.yaml')
if not os.path.exists(user_file):
    print("Input users.yaml file doesn't exit, fallback to the default")
    sys.exit(0)

users = []
with open(user_file) as f:
    users = yaml.load(f)

for user in users['users']:
    key = RSA.generate(2048)
    user['private_key'] = key.exportKey('PEM')
    pubkey = key.publickey()
    user['public_key'] = pubkey.exportKey('OpenSSH')

with open(os.path.join(base_dir, 'users-generated.yaml'), 'w') as f:
    f.write(yaml.dump(users))
