#!/usr/bin/env bash

set -x
set -e

# Create a partition at the end
#   $1 raw disk file
#   $2 percentage to use in the remaining part
create_part(){
  RAW=$1
  PERCENT=$2
  SIZE_B=$((3 * 1024 * 1024 * 1024))
  # Add the partition
  end=$(parted -s $RAW unit B print |grep primary | awk '{print $3}' | tail -n1)
  bytes=${end/B/}
  percent=$(($bytes * 100))
  percent=$(($percent / $SIZE_B))
  start_percent=$(($percent + 1))
  parted -- $RAW unit B mkpart primary ${start_percent}% $PERCENT%
  # Get the last partition start
  start=$(parted -s $RAW unit B print |grep primary | awk '{print $2}' | tail -n1)
  start=${start/B/}

  echo $start
}

# Mount a partition from an image file, identified by its label and copy the
# content of the directory
#   $1 raw disk file
#   $2 label to look for
#   $3 directory whose directory will be copied
copy_directory_in(){
  RAW=$1
  LABEL=$2
  DIRECTORY=$3

  kpartx -a $RAW
  loopdev=$(losetup --noheadings -l | grep "$RAW" | awk '{print $1}' | tail -n1)
  sleep 2
  device=$(lsblk --noheadings --list --output NAME,LABEL $loopdev | grep $LABEL | awk '{print $1}')
  if [ "$device" == "" ]
  then
    echo "No partition with $LABEL found"
    exit 1
  fi
  mkdir -p /tmp/$LABEL
  mount /dev/mapper/$device /tmp/$LABEL
  rm -rf /tmp/$LABEL/*
  cp -r $DIRECTORY/* /tmp/$LABEL/.
  umount /tmp/$LABEL
  kpartx -d $loopdev
  losetup -D
}
