#!/usr/bin/env bash

# 
# Common function variables
#

ENV_GIT_SSH_COMMAND="$(which ssh) -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"

log_file="/tmp/git_backup"
log(){
  date=$(date)
  echo "[$date][$1] $2 " >> "$log_file-$user.log"
}

get_origin(){
  user=$1
  git_address=$2
  git_port=$3
  echo "ssh://git@${git_address}:${git_port}/$user/capes2017.git"
}
