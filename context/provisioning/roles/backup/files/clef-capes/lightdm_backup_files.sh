#!/usr/bin/env bash

# lightdm scripts run as root but 
# set $USER and $HOME 

sudo -u $USER /opt/clef-capes/sync_repository.sh $USER $HOME

# Don't block the login if an error occur
exit 0
