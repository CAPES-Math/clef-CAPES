#!/usr/bin/env bash

# This script initialize the repository
# Committing initial content
# Pull remote content and gives priority to the local changes
# Push  the resulting content

CONFIG_FILE=/opt/clef-capes/config.sh

if [ -e "$CONFIG_FILE" ]
then
  source $CONFIG_FILE
else
  echo "No config file $CONFIG_FILE found"
  exit 127
fi

CONFIG_FILE=/opt/clef-capes/common.sh

if [ -e "$CONFIG_FILE" ]
then
  source $CONFIG_FILE
else
  echo "No config file $CONFIG_FILE found"
  exit 127
fi

user=$1

if [ "$user" == "" ]
then
	log $user "User cannot be empty"
	exit 1
fi

if [ "$user" == "root" ]
then
	log $user "Not backuping root directory"
	exit 1
fi

repository=$2

if [ "$repository" == "" ]
then
	log $user "Repository cannot be empty"
	exit 1
fi


# commit message
message="$(date) : Automatic backup"

pushd $repository

log  $user "(Re)Initializing git repository in $userhome"
git init
git config --global user.email $user@capes2017.fr
git config --global user.name $user

status=$(git status --porcelain | wc -l)
log $user $status
if [ "$status" -eq "0" ]
then
  log $user "Nothing to backup in $userhome"
else
  git add -A .
  git commit -m "$message"
  log $user "Changes commited $status"
fi

# Adding the remote and avoid an error if it already exists
remote=$(get_origin $user $GIT_ADDRESS $GIT_PORT)
git remote add origin $remote || true

# Test the the connection
nc -v -w 1 $GIT_ADDRESS $GIT_PORT < /dev/null
if [ "$?" -ne "0" ]
then
  log $user "Remote is unreachable...skipping"
  exit 1
fi

# Recovering any files updated
# NOTE(msimonin): we are keeping the local modification in case of conflict
GIT_SSH_COMMAND="$ENV_GIT_SSH_COMMAND" git pull --rebase -Xtheirs origin master

# Note(msimonin) make sure the StrictHostKeyCheck is disabled
GIT_SSH_COMMAND="$ENV_GIT_SSH_COMMAND" git push origin master
log $user "Changes pushed $status to $remote"

DISPLAY=:0 sudo -u $user notify-send "Sauvegarde effectuée ($status)"
log $user "End of backup"

popd

exit 0
