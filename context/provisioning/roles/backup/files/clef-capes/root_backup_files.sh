#!/usr/bin/env bash

# NOTE(msimonin) the following doesn't care if several users are connected
user=$(who | grep -v root | awk '{print $1}' | head -n1)

sudo -u $user /opt/clef-capes/sync_repository.sh $user /home/$user
