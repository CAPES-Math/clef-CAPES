#!/bin/sh

SCRIPT_DIR=$(cd "$(dirname "$0")" && pwd)

HOME=/root ansible-playbook -vvv -i $SCRIPT_DIR/provisioning/hosts $SCRIPT_DIR/provisioning/site.yml
